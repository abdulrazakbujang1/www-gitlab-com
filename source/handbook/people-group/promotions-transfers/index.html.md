---
layout: handbook-page-toc
title: "Promotions and Transfers"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

At GitLab, we encourage team members to take control of their own career advancement. For all title changes, the team member should first speak with their manager to discuss a personal [professional development plan](/handbook/people-group/learning-and-development/#career-mapping-and-development). If you feel your title is not aligned with your skill level, come prepared with an explanation of how you believe you meet the proposed level and can satisfy the business need.

As a manager, please follow the following processes, discuss any proposed changes with either the People Business Partner or Chief People Officer, and do not make promises to the team member before the approvals are completed.

We recorded a training on this subject:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TNPLiYePJZ8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Offer Process in BambooHR or Greenhouse

* Promotions for Individual Contributors in the same job family: reviewed and approved through the [BambooHR Promotion Process](/handbook/people-group/promotions-transfers/#promotions--compensation-changes)
* Promotions or Applications to Manager level roles: All managers will need to apply for the open position in [Greenhouse](/handbook/hiring/offers/). They will go through the interview and approval process in Greenhouse to ensure anyone who would want to apply has the opportunity.
* Lateral Transfers to a different job family: apply and approval through the Greenhouse [hiring process](/handbook/hiring/).
* Promotions to Director and above: approval through the [BambooHR Promotion Process](/handbook/people-group/promotions-transfers/#promotions--compensation-changes) to ensure the CFO/CEO have insight and approval into leadership promotions.

## Promotions & Compensation Changes

Any changes to title and compensation should be pushed through as quickly as possible. To promote or change compensation for one of your direct reports, a manager should follow the following steps:

- Create a Google Doc
  * Promotions: The manager (or team member) will create a path to promotion google doc which outlines the criteria of the role.  (i.e. promote based on performance, not based on potential). The promotion document should be viewable and editable by all. If the criteria for promotion is not adequately described on the relevant vacancy description page, then work on that first.
    * Try to remove feelings and use evidence; links to MRs, issues, or project work that clearly demonstrates the skills, knowledge and abilities of the person. There will be situations when an employee is ready to move to the next level through a promotion, however due to the nature of the business that particular role or next level may not be available for a business reasons.  Example the employee is ready for a Manager or Director role, however the business does not have the need, budget or scope for an additional manager/director at that time. The position may or may not become available in the future but it is not a guarantee.
    * [Values](/handbook/values/) alignment is also very important when considering promotions. This is especially the case for anyone who is moving to Senior. As one moves into management, the importance compounds.  Values thrive at a company only when we take them into consideration for hiring and promotions.
    * For promotions into a People Management role, you should ensure the team member is prepared for management. This includes, at a minimum,  the ability and willingness to provide honest and direct performance feedback and coaching, interviewing candidates with an ability to be decisive, ability to have difficult conversations, and the ability to set direction, inspire, and motivate the team.
  * Compensation Only: Create a google doc outlining the reasons for the compensation increase also tying the rationale to our values and the vacancy description. Do not add compensation values to the google doc - only reasoning. Compensation values go into Bamboo only. Make the google doc accessible to anyone at GitLab who has the link.
- Complete an Experience Factor Worksheet
  * The worksheet should be completed by the manager in line with the [experience factor guidelines](/handbook/people-group/global-compensation/#experience-factor) by reviewing the team member with the new leveling criteria. For example, if the team member is promoted from intermediate to senior, a senior experience factor worksheet would be used to influence compensation for the promotion. This also helps to ensure that the team member is fully aware of any new expectations that come with the promotion.
- If the vacancy is being advertised via the [jobs page](/jobs/) the individual must submit an application for the role in order to be compliant with global anti-discrimination laws. Similarly, if there is no vacancy posting, one must be created and shared on the company call so that everyone has the opportunity to apply and be considered.
- Approval Process
  * If the position is a Director (or above) the promotion doc needs to be reviewed by the [e-group](/handbook/leadership/#e-group) in their weekly meeting **3 months before the promotion is intended to take place** and feedback may be provided to the individual and their manager to work on. This is to ensure consistent leadership qualities across the company. Exceptions to this timeline can be made at the e-group's discretion. Anyone who is mid-process as this rule goes into effect (March 2019) does not need to have 3 months added to their timeline but may receive the feedback, if any. We do not yet have a similar method to ensure consistency in external hires for Director (and above) roles. But we hope to add it soon.
  * The Direct Manager will submit the request in BambooHR
    1. Login to BambooHR.
    1. Select the team member you would like to adjust.
    1. In the top right hand corner, click Request a Change.
    1. Select which type of change you are requesting. If you are only looking to adjust compensation, then select Compensation. If this is a promotion that includes a title change and a compensation change, select Promotion. To only change the title, select Job Information.
    1. Enter in all applicable fields in the form, and then submit.
      * Note: Salary information is entered per payroll. For example, if the team member is in the United States, the annual salary is divided by 24 to get the pay rate to be entered. If the employee is in the Netherlands, please divide by 12.96. The divisor can be found in the "Pay Frequency" field above "Comment for the Approver(s)" in the request form. For any questions on how to fill out the form, please reach out to People Ops Analysts.
      1. In the comments section please include a link to a google doc and experience factor worksheet for all requests (promotion, compensation only, etc.) outlining the reasons for the proposed change. Please do not include salary information in the doc so it can be shared on the company call, if applicable.
      1. If there is a change to variable compensation, please fill out the "OTE Change" request separately.
      2. Managers should not communicate any promotion or salary adjustment until the request has gone through the entire approval process and you receive an adjustment letter from People Operations Analysts.
  * The request is next reviewed by the indirect manager in BambooHR.
  * Next the People Business Partner will review the business case for promotion and experience factor worksheet. When those items are complete, they will ping the Compensation & Benefits Manager in Slack to add the compensation notes. The Compensation & Benefits Manager will ensure the proposal adheres to the Global Compensation Calculator, with a comment outlining old compensation, new compensation, increase percentage, additional [stock options](/handbook/stock-options/#stock-option-grant-levels), experience factor, and a link to the salary calculator. The People Business Partner will then approve the request if applicable.
  * Next, the Functional Group leader will review the entire proposal in BambooHR for approval.
  * Next, the CFO or CEO will review the request. If there are any questions, the CFO or CEO will add a comment outlining the specific concerns and the People Operations Analyst will ensure follow-up within a week to escalate to the comp group or deny the request.
- If the request is approved, the People Operations Analyst will process the change and notify the manager.
- Promotions are then announced by the manager on the company call; where the manager describes how the individual met the promotion criteria and includes a link to the merge request where the individual's title is updated on the team page.
- When announcing or discussing a promotion on Slack, please include a link the promotion Google Doc to increase visibility for the reasons behind the promotion.
- If some amount of onboarding in the new role or offboarding from the old role is required (for example a change in access levels to infrastructure systems; switch in groups and email aliases, etc.), People Operations Analysts will notify People Operations Specialists in the internal Promotions/Transfers spreadsheet tracker, and the People Operations Specialists will create an associated [Internal Transition Issue] (https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/internal_transition.md) to list and track associated tasks for the previous and new manager. The previous manager will be prompted to create an [Access Removal Request Issue](https://gitlab.com/gitlab-com/access-requests/blob/master/.gitlab/issue_templates/Access%20Removal%20Request.md) and the new manager will create  an [Access Request Issue](https://gitlab.com/gitlab-com/access-requests) to ensure the correct access is given for the new role and deprovisioned for the previous role, if need be.

### Processing Promotion or Transfer Changes

If the request is approved through BambooHR the People Operations Analyst will create the Letter of Adjustment whereas if the request is through Greenhouse the People Operations Analyst will be notified via the Compensation team email inbox that the letter has been signed. If this is the case, only data systems will need to be updated.


1.  If the request comes through BambooHR, approve the request, then update the entries in BambooHR to ensure that there are the proper dates, amounts and job information. Also, ensure to add stock options to the benefits tab if applicable. If the team member is moving to a Manager position, update their access level in BambooHR.
2.  Notify Payroll of the changes. This can be done in the following google docs: United States: "Payroll Changes", Everyone else: "Monthly payroll changes for non-US international team members". Payroll does not need to be notified for Contractors.
3.  Update the compensation calculator backend spreadsheet.
4.  If the team member is in Sales or transferred to Sales, update the changes tab on the "Final Sales OTE FY 2020" google doc.
5.  Make a copy of the Letter of Adjustment template and enter all applicable information based on the BambooHR request. The effective date is either the 1st or 16th of the month. For sales personnel with a variable change, the effective date is always the 1st of the month.
6. Stage the letter in HelloSign and add the following team members to sign:
   * Add checkbox for the People Operations Analyst to audit
   * Add checkbox for the Manager to communicate the change to the team member and announce at the company calculator
   * Add signature field for the Compensation and Benefits Manager
   * Add signature field for the team member
   * Add sign date field for the team member
7.  Once signed by all parties, save the letter to the “Contracts & Changes” folder in BambooHR.
8.  Notify the People Operations Specialist to open a Transition issue by tagging them in the "Promotion/Transfer Tracker" google doc.


## Demotions

To demote one of your direct reports, a manager should follow the following steps:

- The manager should discuss any performance issues or possible demotions with the People Business Partner in their scheduled meetings with a corresponding google doc.
- To initiate the process, the manager must obtain agreement from two levels of management.
- Proposed changes to a current vacancy description or a new vacancy description should be delivered with request for approval by the second level manager and the People Ops Manager.
- Demotions should also include a review of [compensation](/handbook/people-group/global-compensation) and [stock options](/handbook/stock-options/#stock-option-grant-levels) in the google doc. Managers should consult with People Operations Analysts on these topics; and of course always adhere to the Global Compensation Calculator.
- Once agreement is reached on the demotion and changes (if any) in compensation, send the google doc to the CEO for final approval.
- Once approved, the manager informs the individual. Please cc compensation@domain once the individual has been informed, to processes the changes in the relevant administrative systems, and stage a [Letter of Adjustment](/handbook/contracts/#letter-of-adjustment).
- Changes in title are announced on the company call.
- The manager will initiate any necessary onboarding or offboarding.

## Department Transfers

If you are interested in a vacancy, regardless of level, outside your department or general career progression, you can apply for a transfer through [Greenhouse](https://boards.greenhouse.io/gitlab) or the internal job board, link found on the #new-vacancies Slack channel.
You will never be denied an opportunity because of your value in your current role.

- If you are interested in a transfer, simply submit an application for the new position. If you are not sure the new role is a good fit, schedule time with the hiring manager to learn more information about the role and the skills needed. If after that conversation you are interested in pursuing the internal opportunity, it is recommended that you inform your current manager of your intent to interview for a new role. While you do not need your their permission to apply to the new role, we encourage you to be transparent with them. Most will appreciate that transparency since it's generally better than learning about your move from someone reaching out to them as a reference check. You can also use this as an opportunity to discuss the feedback that would be given to the potential new manager were they to seek it regarding your performance from your current and/or past managers. We understand that the desire to transfer may be related to various factors. If the factor is a desire NOT to work with your current manager, this can be a difficult conversation to have and shouldn't prevent you from pursuing a new role at GitLab.
- Transfers must go through the application process for the new position by applying on the [jobs page](/jobs). The team member will go through the entire interview process outlined on the vacancy description, excluding behavioral or "values alignment" interviews. If you have any questions about the role or the process, please reach out to your functional group's People Business Partner: Julie Armendariz for Sales and G&A; Jessica Mitchell for Marketing, Product, and Alliances; Carol Teskey (interim) for Engineering. In all cases, the applicable People Business Partner should be informed via email, before a transfer is confirmed.
- In the case of transfers, it is expected and required that the gaining manager will check with internal references at GitLab, including previous and current managers.
- Before the offer is made the recruiter will confirm with the team member and the gaining manager that they have indeed reached out to the current manager.  They will discuss the new internal opportunity and that an offer will be made to the team member.
- Recruiting team will ensure that, if applicable, the position has been posted for at least three business days before an offer is made.
- [Compensation](/handbook/people-group/global-compensation) and [stock options](/handbook/stock-options/#stock-option-grant-levels) may be reviewed during the hiring process to reflect the new level and position.
- If after interview, the manager and the GitLab team-member want to proceed with the transfer, internal references should be checked. While a manager cannot block a transfer, there is often good feedback that can help inform the decision. It is advised that the GitLab team-member talk to their manager to explain their preference for the new team and to understand the feedback that will be given to the new manager. It should also be noted, that performance requirements are not always equal across roles, so if a GitLab team-member struggles in one role, those weakness may not be as pronounced in the new role, and vice versa.  However, if there are systemic performance problems unrelated to the specific role or team, a transfer is not the right solution.
- A new [contract](/handbook/contracts/#employee-contractor-agreements) will be sent out following the hiring process.
- If the GitLab team-member is chosen for the new role, the managers should agree on an reasonable and speedy transfer plan. 2 weeks is a usually a reasonable period, but good judgment should be used on completing the transfer in a way that is the best interest of the company, and the impacted people and projects.
- If the team member is transferred, the new manager will announce on the company call and begin any additional onboarding or offboarding necessary.

## Department Transfers Manager Initiated

If you are a manager wishing to recruit someone, the process is the same as an employee initiated transfer.  We encourage the recruiting manager to be transparent with the employee's current manager.  This will allow the current manager the maximal amount of time to plan for the transfer and speed up the overall process.  

### Internal Department Transfers

If you are interested in another position within your department and the manager is also your manager you must do the following;

- Present your proposition to your manager with a google doc.
- If the vacancy is advertised on the [jobs page](/jobs/), to be considered, you must submit an application. If there is no vacancy posting, one must be created and shared in the #new-vacancies channel so that everyone has the opportunity to apply and be considered.
- The manager will asses the function requirements; each level should be defined in the vacancy description.
- If approved, your manager will need to obtain approval from their manager, through the chain of command to the CEO.
- [Compensation](/handbook/people-group/global-compensation) and [stock options](/handbook/stock-options/#stock-option-grant-levels) will be reevaluated to ensure it adheres to the compensation calculator. Don't send the proposal to the CEO until this part is included.
- If the team member is transferred, the manager will announce on the company call and begin any additional onboarding or offboarding necessary.

**When promotion is a consideration - Within Same Job Family**

If a team member sees a vacancy posted that is the next level up within their [job family](/handbook/hiring/#definitions) (for example an Intermediate Frontend Engineer sees a vacancy for an Senior Frontend Engineer), the team member should have a conversation with their manager about exploring that opportunity.  Once that discussion is completed the team member should follow the internal department transfers guidance above.

It is the manager’s responsibility to be honest with the team member about their performance as it relates their promotion readiness. If the manager agrees that the team member is ready, then they will be promoted to the next level. If they do not think the team member is ready for the promotion, they should walk through the experience factor worksheet and their career development document, as well as work on a promotion plan with the team member. The manager should be clear that the team member is not ready for the promotion at this time and what they need to work on. If the team member would still like to submit an application for the role after the conversation with their manager, they can apply and go through the same interview process as external candidates. The recruiter will confirm with the manager that the promotion readiness conversation has taken place before the internal interview process starts.

**For Internal Applicants - Different Job Family**

If the role is in a completely different job family (within their own division or in a completely different division, for example, if a Product Designer is interested in a Product Manager role), the team member must submit an application via the posting on GitLab’s [internal job board](https://app2.greenhouse.io/internal_job_board) on Greenhouse.

After the team member applies, the recruiter will reach out to the team member to connect regarding compensation for the role. In some cases, the compensation may be lower than the current one. Once the team member understands and agrees with the compensation for the new role, they can continue the interview process.

Internal and external candidates will have the same process with the same amount of interviews and when possible the same interviewers, with the exception of the full screening call (which will be instead a short conversation to discuss compensation, as mentioned above). However, if the internal applicant will be staying in the same division and the executive level interview is a part of the process, the executive may choose to skip their interview. All interview feedback and notes will be captured in the internal team member’s Greenhouse profile, which will be automatically hidden from the team member. After interviews are completed, internal “reference checks” will be completed with the applicant’s current manager by the new hiring manager.

It is recommended that team members inform their manager of their desire to move internally and their career aspirations. Your manager should not hear about your new opportunity from the new hiring manager; it should come from you prior to the new hiring manager checking in for references with the current manager.

If you are unsure of the role, set up a coffee chat with the hiring manager to introduce yourself. Express your interest in the role and your desire to learn more about the vacancy requirements and skills needed. If after that conversation you do not feel that you are qualified or comfortable making the move, ask the hiring manager to provide guidance on what you can do to develop yourself so you will be ready for the next opportunity. It may also be possible to set up an [interning for learning](/handbook/people-group/promotions-transfers/#interning-for-learning) situation with the hiring manager.

**For People Ops & Recruiting Team**

Vacancies will be posted both [externally and internally](/handbook/hiring/vacancies/#vacancy-creation-process) using Greenhouse for at least 3 business days.

Once an internal candidate completes their interviews and the internal references are completed, an offer needs to be submitted and approved in Greenhouse by working with the new manager, the People Business Partner, and the People Ops Analyst. Once the offer is approved, the offer will be extended verbally to the internal candidate by the hiring manager for the role.

After the offer is approved and extended by the hiring manager, the Recruiter will follow up with an email containing the details, and the Candidate Experience Specialist will prepare and send a Letter of Adjustment (LOA) through Greenhouse for the GitLab Signatory and the internal candidate to sign.

Once the contract is signed, the Candidate Experience Specialist will move the internal candidate to “Hired” in Greenhouse, making sure to **not** export to BambooHR.

After the internal candidate is moved to “Hired”, the recruiting team will notify the People Operations Analyst who will [update BambooHR]((/handbook/people-group/promotions-transfers/#processing-promotion-or-transfer-changes) with appropriate changes.

### Leveling Up Your Skills

There are a number of different ways to enhance or add to your skill-set at GitLab, for example, if you do not feel like you meet the requirements for an inter-department transfer, discuss with your manager about allocating time to achieve this. This can be at any level. If you're learning to program, but aren't sure how to make the leap to becoming a developer, you could contribute to an open-source project like GitLab in your own time.

As detailed in our [Learning & Development Handbook](/handbook/people-group/learning-and-development/), team members will be supported in developing their skills and increasing their knowledge even if promotion is not their end goal.

### Interning for Learning

If your manager has coverage, you can spend a percentage of your time working ('interning') with another team.

This could be for any reason: maybe you want to broaden your skills or maybe you've done that work before and find it interesting.

If your team has someone working part-time on it, it's on the manager of that team to ensure that the person has the support and training they need, so they don't get stuck. Maybe that's a buddy system, or maybe it's just encouragement to use existing Slack channels - whatever works for the individuals involved.

#### How does this work?

**What percentage of time should be allocated?** Well, 10% time and 20% time are reasonably common. As long as you and your manager have the capacity the decision is theirs and yours.

**What about the team losing a person for X% of the time? How are they supposed to get work done?**
Each manager needs to manage the capacity of their team appropriately. If all of the team are 'at work' (no one is on PTO, or parental leave, or off sick), and the team still can't afford X% of one person's time - that team might be over capacity.

**Can I join a team where I have no experience or skills in that area?**
That's up to the managers involved. It may be that the first step is to spend some time without producing anything in particular - in which case, it's possible that the [tuition reimbursement policy](/handbook/people-group/code-of-conduct/#tuition-reimbursement) may be a better fit (Or it might not.)

**How long does an internship of this nature last?**
This will vary from team to team, but typically 6 weeks to 3 months depending on the goals for your internship.

**This sounds great but how do I initiate this process?**
First step is to discuss this with your manager at your next 1:1. Come prepared with your proposal highlighting what skills you want to learn/enhance and the amount of time you think you will need. Remember, this should be of benefit to you and GitLab. You and your manager will need to collaborate on how you both can make this happen which may also involve discussing it further with the manager of the team you may be looking to transfer to. All discussions will be done transparently with you. Be mindful though that the business needs may mean a move can't happen immediately.

**How do I find a mentor?**
On the [team page](/company/team), you can see who is willing to be a mentor by looking at the associated [expertise](/company/team/structure/#expert) on their entry.

**Does completing an internship guarantee me a role on the team?**
Completing an internship through this program does not guarantee an internal transfer. For example, there may not be enough
allocated headcount in the time-frame in which you complete your internship.

If at the end of your internship, you are interested in transferring teams please follow the guidelines in [Internal Department Transfers](#internal-department-transfers).


#### Starting your new interning role

Please create a new issue in the [Training project](https://gitlab.com/gitlab-com/people-ops/Training/issues/new), choose the `interning_for_learning` template, and fill out the placeholders.
Here is [an example](https://docs.google.com/document/d/1Oo7gnsFN5t_tQWgkYUqx3TWVRjlIiFvcG0veTabPH4g/edit#heading=h.vp8zbds2lyqa).

Once you've agreed upon the internship goals, both managers should inform their respective groups' People Business Partner.

#### Recommendations

We recommend that, at any given time, each [team](/company/team/structure/#team-and-team-members) is handling only one intern. This is to allow for an efficient and focused mentorship without impacting the capacity of the team. You can, of course, adjust this depending on the size of the team but please consider the impact of mentoring when scheduling internships.

## Specialty Change

If a team member is staying on the current benchmark, but changing their specialty (moving from plan to secure for example), the only request needed is a Job Information Change in BambooHR. This request will be submitted by the current manager. There is no impact to compensation or leveling. If the current manager needs to backfill the role, please reach out to the recruiting team.
