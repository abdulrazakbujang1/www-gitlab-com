---
layout: handbook-page-toc
title: "IAM.1.06 - Shared Logical Accounts Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# IAM.1.06 - Shared Logical Accounts

## Control Statement

GitLab restricts the use of shared and group authentication credentials. Authentication credentials for shared and group accounts are reset quarterly.

## Context

Adding restrictions to the use of shared accounts help protect against malicious activity and stolen accounts. Shared accounts are more likely to be compromised because the account credentials are shared with multiple people and it's often not feasible to use multi-factor authentication. While shared accounts sometimes must be used, this control aims to limit account sharing to only those cases where it's necessary and no good alternatives exist, and to add safeguards to when they are used.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

Control onwership:
* `IT Operations`

Process ownership:
* IT-Ops 

## Guidance

An exception to the policy to define the types of services approved for shared accounts and a process for the lifecycle of the access and a mechanism to alert the appropriate teams that authentication credentials must be reset (e.g., email alerts, an issue, calendar event, etc).

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Shared Logical Accounts control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/810).

### Policy Reference

## Framework Mapping

* SOC2 CC
  * CC6.1
