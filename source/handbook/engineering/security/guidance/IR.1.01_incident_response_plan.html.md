---
layout: handbook-page-toc
title: "IR.1.01 - Incident Response Plan Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# IR.1.01 - Incident Response Plan

## Control Statement

GitLab defines the types of incidents that need to be managed, tracked and reported, including:

* Procedures for the identification and management of incidents
* Procedures for the resolution of confirmed incidents
* Key incident response systems
* Incident coordination and communication strategy
* Contact method for internal parties to report incidents
* Support team contact information
* Notification to relevant management in the event of a security breach
* Provisions for updating and communicating the plan
* Provisions for training of support team
* Preservation of incident information
* Management review and approval, quarterly, or when major changes to the organization occur

## Context

The purpose of this control is to ensure GitLab creates, implements, and maintains an effective plan to identify, resolve, and prevent incidents within its application, systems, and services. By having an organized and continually evolving incident response plan, GitLab can maintain the availability, reliability, performance, and confidentiality offered to GitLab customers, GitLab team-members, and partners.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

* Control Owner: `Infrastructure`
* Process owner(s):
    * Security Operations`
    * Infrastructure

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Incident Response Plan control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/839).

Examples of evidence an auditor might request to satisfy this control:

* Provide copies of the Incident Response pages, which are linked to and described below
* Provide sample reports and other outputs of the various functions listed below, such as Infrastructure and/or Security incident issues

### Policy Reference

## Framework Mapping

* ISO
  * A.16.1.1
  * A.16.1.2
  * A.16.1.4
  * A.16.1.5
  * A.16.1.6
  * A.16.1.7
* SOC2 CC
  * CC7.4
  * CC7.5
* PCI
  * 11.1.2
  * 11.5.1
  * 12.10
  * 12.10.1
  * 12.10.4
  * 12.10.5
  * 12.10.6
