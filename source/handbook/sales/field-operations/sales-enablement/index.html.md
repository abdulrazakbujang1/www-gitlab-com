---
layout: handbook-page-toc
title: "Sales Enablement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### **HOW TO COMMUNICATE WITH US**
Slack: @sales-enablement

### **CHARTER**

Ensure everyone in the GitLab sales, partner, and customer ecosystem has the right skills, knowledge, and resources to meet and exceed desired business objectives in accordance with [Gitlab’s values](/handbook/values/).

### **KEY PROGRAMS**

*  [Customer Enablement](/handbook/sales/field-operations/customer-enablement/)
*  [Sales Onboarding](/handbook/sales/onboarding/)
*  [GitLab Command of the Message](/handbook/marketing/product-marketing/sales-resources/#command-of-the-message)
*  [Continuous Improvement / Continuous Development for Sales (CI/CD for Sales)](/handbook/sales/training/)

### **HANDBOOK-FIRST APPROACH TO GITLAB LEARNING AND DEVELOPMENT MATERIALS**
Chat between David Somers (Director, Sales and Customer Enablement) and Sid Sijbrandij (CEO)
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oXTZQpICxeE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Key Discussion points:
*  Our [Mission](/company/strategy/#mission) is that Everyone Can Contribute, and our most important value is [Results](/handbook/values/#results). Like we've extended that to the Handbook, we want to extend it to our Learning Materials.
*  We want to leverage the best of an e-learning platform, with the benefits of reminders, interactivity, and more but make sure the materials we produce are also available to those who aren't using an e-learning platform, while fulfilling [our mission](/company/strategy/#mission). 
*  There are benefits to keeping our e-learning material [handbook-first](/handbook/handbook-usage/#why-handbook-first):
   * Folks who have already completed a formal training through an e-learning platform may want to return to the materials
   * Those who never go through the formal platform may also benefit from the materials
   * The handbook continues to be the SSOT, with the e-learning platform leveraging handbook materials through screenshots, embeds, and more

### **WORKING WITH SALES AND CUSTOMER ENABLEMENT**
*  [Six Critical Questions](https://docs.google.com/document/d/16s9OzdS9HhD_CLVYLEjoK2SKfgMNyjUBibmsvwOQ_js/edit?usp=sharing) (inspired by _The Advantage: Why Organizational Health Trumps Everything Else in Business_ by Patrick Lencioni)
*  [Sales and Customer Enablement groups, projects, and labels](https://docs.google.com/document/d/1dVXa0YAFwa1DjrUWIxZCatZnn3MdrlNuRUJCw9BgiI4/edit?usp=sharing)

