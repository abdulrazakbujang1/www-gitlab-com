---
layout: handbook-page-toc
title: "Data Team"
description: "GitLab Data Team Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

## <i class="fab fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i>Quick Links
[Primary Project](https://gitlab.com/gitlab-data/analytics/){:.btn .btn-purple-inv}
[dbt docs](https://gitlab-data.gitlab.io/analytics/dbt/snowflake/#!/overview){:.btn .btn-purple-inv}
[Periscope](https://app.periscopedata.com/app/gitlab/){:.btn .btn-purple-inv}
[Epics](https://gitlab.com/groups/gitlab-data/-/roadmap?layout=MONTHS&sort=start_date_asc){:.btn .btn-purple}
[OKRs](/company/okrs/){:.btn .btn-purple}
[GitLab Unfiltered YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI){:.btn .btn-purple}

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i>Data Team Handbook
 * [Data Infrastructure](/handbook/business-ops/data-team/data-infrastructure/)
 * [Data Quality](/handbook/business-ops/data-team/data-quality/)
 * [Data Quality Process](/handbook/business-ops/data-team/data-quality-process/)
 * [KPI Definition Mapping](/handbook/business-ops/data-team/metrics/)
 * [Periscope](/handbook/business-ops/data-team/periscope/)
 * [Python Style Guide](/handbook/business-ops/data-team/python-style-guide)
 * [SQL Style Guide](/handbook/business-ops/data-team/sql-style-guide)

----

## <i class="fas fa-bullhorn fa-fw icon-color font-awesome" aria-hidden="true"></i>Contact Us

* [Data Team Project](https://gitlab.com/gitlab-data/analytics/)
* [#data](https://gitlab.slack.com/messages/data/) on Slack
* [What time is it for folks on the data team?](https://www.worldtimebuddy.com/?pl=1&lid=2950159,6173331,4487042,4644585&h=2950159)

#### Slack

The Data Team uses these channels on Slack:

* [#data](https://gitlab.slack.com/messages/data/) is the primary channel for all of GitLab's data and analysis conversations. This is where folks from other teams can link to their issues, ask for help, direction, and get general feedback from members of the Data Team.
* [#data-daily](https://gitlab.slack.com/messages/data-daily/) is where the Data Team tracks day-to-day productivity, blockers, and fun. Powered by [Geekbot](https://geekbot.com/), it's our asynchronous version of a daily stand-up, and helps keep everyone on the Data Team aligned and informed.
* [#data-lounge](https://gitlab.slack.com/messages/data-lounge/) is for links to interesting articles, podcasts, blog posts, etc. A good space for casual data conversations that don't necessarily relate to GitLab.
* [#business-operations](https://gitlab.slack.com/messages/business-operations/) is where the Data Team coordinates with Business Operations in order to support scaling, and where all Business Operations-related conversations occur.
* [#analytics-pipelines](https://gitlab.slack.com/messages/analytics-pipelines/) is where slack logs for the ELT pipelines are output and is for data engineers to maintain.
* [#dbt-runs](https://gitlab.slack.com/messages/dbt-runs/), like #analytics-pipelines, is where slack logs for [dbt](https://www.getdbt.com/) runs are output. The Data Team tracks these logs and triages accordingly.
* [#data-triage](https://gitlab.slack.com/messages/data-triage/) is an activity feed of opened and closed issues and MR in the data team project.

#### Meetings

The [Data Team's Google Calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9kN2RsNDU3ZnJyOHA1OHBuM2s2M2VidW84b0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) is the SSOT for meetings.
It also includes relevant events in the data space.
Anyone can add events to it.
Many of the events on this calendar, including Monthly Key Reviews, do not require attendance and are FYI events.
When creating an event for the entire Data Team, it might be helpful to consult the [Time Blackout sheet](https://docs.google.com/spreadsheets/d/1-L1l1x0uayj-bA_U9ETt2w12rC8SqC5mHP7Aa-rkmsY/edit#gid=0).

The Data Team has the following recurring meetings:
* Data Ops - Meeting includes Data team, Finance Business Partner, SalesOps Liaison, and FP&A, though anyone is welcome. It occurs weekly on Tuesdays at 1600 UTC.
* Milestone Pointing - Meeting includes Data team members; specialized analysts are required only for the first part of the meeting focusing on housekeeping issues. Meeting is for pointing the upcoming milestone. See more details in the Milestone planning section. Meeting occurs every other Tuesday at 1700 UTC.
* Social Calls- Social calls have no agenda. They are 30 minutes weekly to catch up on life and other occurrences. They occur every Tuesday at 1800 UTC.

##### Meeting Tuesday
The team honors *Meeting Tuesday*.
We aim to consolidate all of our meetings into Tuesday, since most team members identify more strongly with the [Maker's Schedule over the Manager's Schedule](http://www.paulgraham.com/makersschedule.html).

<div class="alert alert-purple center">
  <p class="purple center" style="font-size: 34px ; text-align: center ; margin: auto">
    <strong>We <i class="fab fa-gitlab orange font-awesome" aria-hidden="true"></i> Data</strong>
  </p>
</div>
{:.no_toc}

## <i class="fas fa-seedling fa-fw icon-color font-awesome" aria-hidden="true"></i>Charter

The Data Team is a part of the Finance organization within GitLab, but we serve the entire company. We do this by maintaining a data warehouse where information from all business systems are stored and managed for analysis.

Our charter and goals are as follows:

* Build and maintain a centralized data warehouse (Snowflake) that can support data integration, aggregation, and analysis requirements from all functional groups within the company
* Create a common data framework and governance practice
* Create and maintain scalable ELT pipelines that support the data sources needed for analysis
* Work with functional groups through [Finance Business Partners](/handbook/finance/financial-planning-and-analysis/#manager-financial-planning--analysis) to establish the single source of truth (SSOT) for company Key Performance Indicators (KPIs)
* Establish a change management process for source systems, data transformations, and reporting
* Develop a Data Architecture plan in conjunction with functional groups
* Develop a roadmap for systems evolution in alignment with the Company’s growth plans
* Collaborate with infrastructure to maintain our self-hosted Snowplow pipeline for web event analytics
* Create and evangelize analyses produced in our business intelligence tool (Periscope); support others learning to and creating their own analyses

## <i class="far fa-compass fa-fw icon-color font-awesome" aria-hidden="true"></i>Data Team Principles

The Data Team at GitLab is working to establish a world-class analytics function by utilizing the tools of DevOps in combination with the core values of GitLab.
We believe that data teams have much to learn from DevOps.
We will work to model good software development best practices and integrate them into our data management and analytics.

A typical data team has members who fall along a spectrum of skills and focus.
For now, the data function at GitLab has Data Engineers and Data Analysts; eventually, the team will include Data Scientists.
Analysts are divided into being part of the Central Data Function and specializing in different functions in the company.

Data Engineers are essentially software engineers who have a particular focus on data movement and orchestration.
The transition to DevOps is typically easier for them because much of their work is done using the command line and scripting languages such as bash and python.
One challenge in particular are data pipelines.
Most pipelines are not well tested, data movement is not typically idempotent, and auditability of history is challenging.

Data Analysts are further from DevOps practices than Data Engineers.
Most analysts use SQL for their analytics and queries, with Python or R.
In the past, data queries and transformations may have been done by custom tooling or software written by other companies.
These tools and approaches share similar traits in that they're likely not version controlled, there are probably few tests around them, and they are difficult to maintain at scale.

Data Scientists are probably furthest from integrating DevOps practices into their work.
Much of their work is done in tools like Jupyter Notebooks or R Studio.
Those who do machine learning create models that are not typically version controlled.
Data management and accessibility is also a concern.

We will work closely with the data and analytics communities to find solutions to these challenges.
Some of the solutions may be cultural in nature, and we aim to be a model for other organizations of how a world-class Data and Analytics team can utilize the best of DevOps for all Data Operations.

Some of our beliefs are:

* Everything can and should be defined in code
* Everything can and should be version controlled
* Data Engineers, Data Analysts, and Data Scientists can and should integrate best practices from DevOps into their workflow
* It is possible to serve the business while having a high-quality, maintainable code base
* Analytics, and the code that supports it, can and should be open source
* There can be a single source of truth for every analytic question within a company
* Data team managers serve their team and not themselves
* [Glue work](https://www.locallyoptimistic.com/post/glue-work/) is important for the health of the team and is recognized individually for the value it provides. [We call this out specifically as women tend to over-index on glue work and it can negatively affects their careers.](https://noidea.dog/glue)

## <i class="far fa-compass fa-fw icon-color font-awesome" aria-hidden="true"></i> Team Organization


The Data Team operates in a hub and spoke model, where some analysts or engineers are part of the central data team (hub) while others are embedded (spoke) or distributed (spoke) throughout the organization.

**Central** - those in this role report to and have their priorities set by the Data team. They currently support those in the Distributed role, cover ad-hoc requests, and support all functional groups (business units).

**Embedded** - those in this role report to the data team but their priorities are set by their functional groups (business units).

**Distributed** - those in this role report to and have their priorities set by their functional groups (business units). However, they work closely with those in the Central role to align on data initiatives and for assistance on the technology stack.

All roles mentioned above have their MRs and dashboards reviews by members in the Data team. Both Embedded and Distributed data analyst or data engineer tend to be subject matter experts (SME) for a particular business unit.


#### Data Support Per Organization

##### Central 

###### Data Engineers 

| Role | Team Member | Type | Prioritization Owners | 
| ------ | ------ | ------ | ------ | 
| Manager, Data | [@jjstark](https://gitlab.com/jjstark) | Central | [@wzabaglio](https://gitlab.com/wzabaglio) | 
| Staff Data Engineer, Architecture | [@tayloramurphy](https://gitlab.com/tayloramurphy) | Central |  [@jjstark](https://gitlab.com/jjstark) |
| Senior Data Engineer | [@tlapiana](https://gitlab.com/tlapiana) | Central | [@jjstark](https://gitlab.com/jjstark) |
| Data Engineer | TBD | Central (Main Focus: Sales & Marketing) | TBD|
| Data Engineer | TBD | Central (Main Focus: Product & Engineering) | TBD|

**Board: TBD**

###### Data Analysts  

| Role | Team Member | Type | Prioritization Owners | Board |
| ------ | ------ | ------ | ------ |  ------ | 
| Manager, Data | [@kathleentam](https://gitlab.com/kathleentam) | Central | [@wzabaglio](https://gitlab.com/wzabaglio) | [Board](https://gitlab.com/groups/gitlab-data/-/boards/1315722?milestone_title=%23started) | 
| Data Analyst | [@derekatwood](https://gitlab.com/derekatwood) | Central | [@kathleentam](https://gitlab.com/kathleentam) |  [Board](https://gitlab.com/groups/gitlab-data/-/boards/1315722?milestone_title=%23started) | 
| Data Analyst | [Click Here to Apply!](https://boards.greenhouse.io/gitlab/jobs/4441205002) | Central (Main Focus: Engineering) | [@kathleentam](https://gitlab.com/kathleentam) |  TBD | 
| Data Analyst | [Click Here to Apply!](https://boards.greenhouse.io/gitlab/jobs/4433341002) | Central (Main Focus: Sales, Marketing, Finance)  | [@kathleentam](https://gitlab.com/kathleentam) |  TBD | 
| Data Analyst | TBD | Central (Main Focus: Product, Growth) | [@kathleentam](https://gitlab.com/kathleentam) |  TBD | 
| Data Analyst | TBD  | Central (Main Focus: Periscope, Corporate, Alliances) | [@kathleentam](https://gitlab.com/kathleentam) |  TBD | 


##### Business Operations 

| Role | Team Member | Type | Prioritization Owners | Board |
| ------ | ------ | ------ | ------ |  ------ | 
| Data Analyst, Operations | TBD | Embedded | [@wzabaglio](https://gitlab.com/wzabaglio) |  TBD | 

##### Finance

| Role | Team Member | Type | Prioritization Owners | 
| ------ | ------ | ------ | ------ | 
| Data Analyst, Finance | [@iweeks](https://gitlab.com/iweeks) | Embedded | [@wwright](https://gitlab.com/wwright) | 

Board: TBD

##### Growth

| Role | Team Member | Type | Prioritization Owners | 
| ------ | ------ | ------ | ------ | 
| Sr. Data Analyst, Growth | [@mpeychet](https://gitlab.com/mpeychet) | Embedded | Primary (DRI): [@sfwgitlab](https://gitlab.com/sfwgitlab);   Secondary: [@timhey](https://gitlab.com/timhey), [@jstava](https://gitlab.com/jstava), [@s_awezec](https://gitlab.com/s_awezec), [@mkarampalas](https://gitlab.com/mkarampalas) | 
| Data Analyst, Growth | [@eli_kastelein](https://gitlab.com/eli_kastelein) | Embedded | Primary (DRI): [@sfwgitlab](https://gitlab.com/sfwgitlab);   Secondary: [@timhey](https://gitlab.com/timhey), [@jstava](https://gitlab.com/jstava), [@s_awezec](https://gitlab.com/s_awezec), [@mkarampalas](https://gitlab.com/mkarampalas) | 

Board: [Growth Board](https://gitlab.com/groups/gitlab-data/-/boards/1082241)

##### People 
This role will support all People and Recruiting data analytics requests. 

| Role | Team Member | Type | Prioritization Owners | 
| ------ | ------ | ------ | ------ | 
| Data Analyst, People | [Click Here to Apply!](https://boards.greenhouse.io/gitlab/jobs/4416681002) | Embedded | TBD | 

Board: TBD

##### Engineering, Infrastruture 

| Role | Team Member | Type | Prioritization Owners | 
| ------ | ------ | ------ | ------ | 
| Staff Data Analyst | [@davis_townsend](https://gitlab.com/davis_townsend) | Distributed | [@glopezfernandez](/glopezfernandez) |

Board: TBD 

##### Marketing

| Role | Team Member | Type | Prioritization Owners | 
| ------ | ------ | ------ | ------ | 
| Marketing Operations Manager | [@rkohnke](https://gitlab.com/rkohnke) | Distributed | [@rkohnke](https://gitlab.com/rkohnke) |
| Data Analyst, Marketing | TBD | Embedded | [@rkohnke](https://gitlab.com/rkohnke) | 

Board: TBD

##### Sales

| Role | Team Member | Type | Prioritization Owners | 
| ------ | ------ | ------ | ------ | 
| Senior Sales Analytics Analyst | [@JMahdi](https://gitlab.com/JMahdi) | Distributed | [@mbenza](https://gitlab.com/mbenza) |
| Senior Sales Analytics Analyst | [@mvilain](https://gitlab.com/mvilain) | Distributed | [@mbenza](https://gitlab.com/mbenza) |
| Senior Sales Analytics Analyst | Start 2019-10-21 | Distributed | [@mbenza](https://gitlab.com/mbenza) |

Board: TBD


## <i class="fas fa-chart-line fa-fw icon-color font-awesome" aria-hidden="true"></i> Data Analysis Process

Analysis usually begins with a question.
A stakeholder will ask a question of the data team by creating an issue in the [Data Team project](https://gitlab.com/gitlab-data/analytics/) using the appropriate template.
The analyst assigned to the project may schedule a discussion with the stakeholder(s) to further understand the needs of the analysis, though the preference is always for async communication.
This meeting will allow for analysts to understand the overall goals of the analysis, not just the singular question being asked, and should be recorded.
All findings should be documented in the issue.
Analysts looking for some place to start the discussion can start by asking:
* How can your favorite reports be improved?
* How do you use this data to make decisions?
* What decisions do you make and what information will help you to make them quicker/better?

An analyst will then update the issue to reflect their understanding of the project at hand.
This may mean turning an existing issue into a meta issue or an epic.
Stakeholders are encouraged to engage on the appropriate issues.
The issue then becomes the SSOT for the status of the project, indicating the milestone to which it has been assigned and the analyst working on it, among other things.
The issue should always contain information on the project's status, including any blockers that can help explain its prioritization.
Barring any confidentiality concerns, the issue is also where the final project will be delivered, after peer/technical review.
When satisfied, the analyst will close the issue.
If the stakeholder would like to request a change after the issue has been closed, s/he should create a new issue and link to the closed issue.

The Data Team can be found in the #data channel on slack.

### Working with the Data Team

1. Once a KPI or other Performance Indicate is defined and assigned a prioritization, the metric will need to be added to Periscope by the data team.
2. **Before** syncing with the data team to add a KPI to Periscope, the metric must be:
    * Clearly defined in the relevant section in the handbook and added to the GitLab KPIs [with all of its parts](/handbook/ceo/kpis/#parts-of-a-kpi).
    * Reviewed with the Financial Business Partner for the group.
    * Approved and reviewed by the executive of the group.
3. Once the KPI is ready to be added into Periscope, create an issue on the [GitLab Data Team Issue Tracker](https://gitlab.com/gitlab-data/analytics/issues) using the [KPI Template](https://gitlab.com/gitlab-data/analytics/blob/master/.gitlab/issue_templates/kpi_template.md) or [PI Request Template](https://gitlab.com/gitlab-data/analytics/blob/master/.gitlab/issue_templates/Visualization%20or%20Dashboard-%20New%20Request.md).
    * The Data team will verify the data sources and help to find a way to automate (if necessary).
    * Once the import is complete, the data team will present the information to the owner of the KPI for approval who will document in the relevant section of the handbook.


### Can I get an update on my dashboard?

The data team's priorities come from our OKRs.
We do our best to service as many of the requests from the organization as possible.
You know that work has started on a request when it has been assigned to a milestone.
Please communicate in the issue about any pressing priorities or timelines that may affect the data team's prioritization decisions.
Please do not DM a member of the data team asking for an update on your request.
Please keep the communication in the issue.

## <i class="fas fa-tasks fa-fw icon-color font-awesome" aria-hidden="true"></i>How we Work

### Documentation

The data team, like the rest of GitLab, works hard to document as much as possible. We believe [this framework](https://www.divio.com/blog/documentation/) for types of documentation from Divio is quite valuable. For the most part, what's captured in the handbook are tutorials, how-to guides, and explanations, while reference documentation lives within in the primary analytics project. We have aspirations to tag our documentation with the appropriate function as well as clearly articulate the [assumed audiences](https://v4.chriskrycho.com/2018/assumed-audiences.html) for each piece of documentation.

### OKR Planning

Data Team OKRs are derived from the higher level BizOps/Finance OKRs as well as the needs of the team.
At the beginning of a FQ, the team will outline all actions that are required to succeed with our KRs *and* in helping other teams measure the success of their KRs.
The best way to do that is via a team brain dump session in which everyone lays out all the steps they anticipate for each of the relevant actions.
This is a great time for the team to raise any blockers or concerns they foresee.
These should be recorded for future reference.

These OKRs drive ~60% of the work that the central data team does in a given quarter.
The remaining time is divided between urgent issues that come up and ad hoc/exploratory analyses.
Specialty data analysts (who have the title "Data Analyst, Specialty") should have a similar break down of planned work to responsive work, but their priorities are set by their specialty manager.

### Milestone Planning

The data team currently works in two-week intervals, called milestones.
Milestones start on Tuesdays and end on Mondays.
This discourages last-minute merging on Fridays and allows the team to have milestone planning meetings at the top of the milestone.

Milestones may be three weeks long if they cover a major holiday or if the majority of the team is on vacation or at Contribute.
As work is assigned to a person and a milestone, it gets a weight assigned to it.

Milestone planning should take into consideration:
* vacation timelines
* conference schedules
* team member availability
* team member work preferences (specialties are different from preferences)

The milestone planning is owned by the Manager, Data. 

The timeline for milestone planning is as follows:
* Meeting Preparation - Responsible Party: Milestone Planner
   * Investigate and flesh out open issues.
   * Assign issues to the milestone based on alignment with the Team Roadmap.
   * Make entry for new milestone in [meeting notes](https://docs.google.com/document/d/1rSHFQfVqE2qgVoBhNAj3TgPnRYsBIG-Ilezghf9-pZc/edit) (GitLab Internal).
   * Note: Issues are not assigned to an individual at this stage, except where required.

| Day | Current Milestone | Next Milestone |
| ---- | ---- | ---- |
| 0 - 1st Tuesday |  **Milestone Start** <br><br>Milestone planner closes the completed milestone <br><br>Milestone planner updates the new milestone name to contain '(current)' <br><br>Milestone planner adds "missed milestone" label to all issues that are rolling over before updating issues to the current milestone | - |
| 6 - 1st Monday | - | Groom new issues for planning |
| 7 - 2nd Tuesday | **Midpoint** <br><br>Any issues that are at risk of slipping from the milestone must be raised by the assignee | **Milestone Review and Planning Meeting** <br><br>Discuss: What we learned from the last milestone. Priorities for new milestone. What's coming down the pike. <br><br>Issues are pointed collectively in the [Milestone Planning Meeting](https://docs.google.com/document/d/1rSHFQfVqE2qgVoBhNAj3TgPnRYsBIG-Ilezghf9-pZc/edit) (GitLab Internal) which now takes place one week before the milestone begins. <br><br>Note: Pointing is done without knowledge of who may pick up the task.| 
| 10 - 2nd Friday | **The last day to submit MRs for review** <br><br>MRs must include documentation and testing to be ready to merge <br><br>No MRs are to be merged on Fridays | **Milestone is roughly final** <br><br>Milestone Planner distributes issues to team members, with the appropriate considerations and preferences |
| 13 - 2nd Monday | **Last day of Milestone** <br><br>Ready MRs can be merged | 

The short-term goal of this process is to improve our ability to plan and estimate work through better understanding of our velocity.
In order to successfully evaluate how we're performing against the plan, any issues not raised at the T+7 mark should not be moved until the next milestone begins.

The work of the data team generally falls into the following categories:
* Infrastructure
* Analytics
   * Central Team
   * Specialist Team
* Housekeeping

During the milestone planning process, we point issues.
Then we pull into the milestone the issues expected to be completed in the timeframe.
Points are a good measure of consistency, as milestone over milestone should share an average.
Then issues are prioritized according to these categories.

Issues are not assigned to individual members of the team, except where necessary, until someone is ready to work on it.
Work is not assigned and then managed into a milestone.
Every person works on the top priority issue for their job type.
As that issue is completed, they can pick up the next highest priority issue.
People will likely be working on no more than 2 issues at a time.

Given the power of the [Ivy Lee](https://jamesclear.com/ivy-lee) method, this allows the team to collectively work on priorities as opposed to creating a backlog for any given person.
As a tradeoff, this also means that every time a central analyst is introduced to a new data source their velocity may temporarily decrease as they come up to speed;
the overall benefit to the organization that any analyst can pick up any issue will compensate for this, though.
Learn [how the product managers groom issues](https://www.youtube.com/watch?v=es-SuhU_6Rc).

Data Engineers will work on Infrastructure issues.
Data Analysts, Central and sometimes Data Engineers work on general Analytics issues.
Data Analysts, <Specialty> work on <Specialty> analyses, e.g Growth, Finance, etc.

There is a demo of [what this proposal would look like in a board](https://gitlab.com/groups/gitlab-data/-/boards/1187725).

This approach has many benefits, including:
1. It helps ensure the highest priority projects are being completed
2. It can help leadership identify issues that are blocked
3. It provides leadership view into the work of the data team, including specialty analysts whose priorities are set from outside the data function
4. It encourages consistent [throughput](/handbook/engineering/management/throughput/) from team members
5. It makes clear to stakeholders where their ask is in priority
6. It helps alleviate the pressure of planning the next milestone, as issues are already ranked

### Issue Types

There are three *general* types of issues:
* Discovery
* Introducing a new data source
* Work

Not all issues will fall into one of these buckets but 85% should.

##### Discovery issues
Some issues may need a discovery period to understand requirements, gather feedback, or explore the work that needs to be done.
Discovery issues are usually 2 points.

##### Introducing a new data source
Introducing a new data source requires a *heavy lift* of understanding that new data source, mapping field names to logic, documenting those, and understanding what issues are being delivered.
Usually introducing a new data source is coupled with replicating an existing dashboard from the other data source.
This helps verify that numbers are accurate and the original data source and the data team's analysis are using the same definitions.

##### Work
This umbrella term helps capture:
* inbound requests from GitLab team-members that usually materialize into a dashboard
* housekeeping improvements/technical debt from the data team
* goals of the data team
* documentation notes

It is the responsibility of the assignee to be clear on what the scope of their issue is.
A well-defined issue has a clearly outlined problem statement. Complex or new issues may also include an outline (not all encompassing list) of what steps need to be taken.
If an issue is not well-scoped as its assigned, it is the responsibility of the assignee to understand how to scope that issue properly and approach the appropriate team members for guidance early in the milestone.

### Issue Pointing

**Issue pointing captures the complexity of an issue, not the time it takes to complete an issue. That is why pointing is independent of who the issue assignee is.**

* Refer to the table below for point values and what they represent.
* We size and point issues as a group.
* Effective pointing requires more fleshed out issues, but that requirement shouldn't keep people from creating issues.
* When pointing work that happens outside of the Data Team projects, add points to the issue in the relevant Data Team project and ensure issues are cross-linked.

| Weight | Description |
| ------ | ------ |
| Null | Meta and Discussions that don't result in an MR |
| 0 | Should not be used. |
| 1 | The simplest possible change including documentation changes. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. |
| 13 | A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break into smaller Issues. |

### Issue Labeling

Think of each of these groups of labels as ways of bucketing the work done. All issues should get the following classes of labels assigned to them:

* Who (Purple): Team for which work is primarily for (Data, Finance, Sales, etc.)
* What - Data or Tool
  * Data (Light Green): Data being touched (Salesforce, Zuora, Zendesk, Gitlab.com, etc.)
  * Tool (Light Blue) (Periscope, dbt, Stitch, Airflow, etc.)
* Where (Brown): Which part of the team performs the work (Analytics, Infrastructure, Housekeeping)
* How (Orange): Type of work (Documentation, Break-fix, Enhancement, Refactor, Testing, Review)

Optional labels that are useful to communicate state or other priority
* State (Red) (Won't Do, Blocked, Needs Consensus, etc.)
* Inbound: For issues created by folks who are not on the data team; not for asks created by data team members on behalf of others

### Daily Standup

Members of the data team use Geekbot for our daily standups.
These are posted in [#data-daily](https://gitlab.slack.com/archives/CGG0VRJJ0/p1553619142000700).
When Geekbot asks, "What are you planning on working on today? Any blockers?" try answering with specific details, so that teammates can proactively unblock you.
Instead of "working on Salesforce stuff", consider "Adding Opportunity Owners for the `sfdc_opportunity_xf` model`."
There is no pressure to respond to Geekbot as soon as it messages you.
Give responses to Geekbot that truly communicate to your team what you're working on that day, so that your team can help you understand if some priority has shifted or there is additional context you may need.

### Merge Request Workflow

*Ideally*, your workflow should be as follows:
1. Confirm you have access to the analytics project. If not, request Developer access so you can create branches, merge requests, and issues.
1. Create an issue or open an existing issue.
1. Add appropriate labels to the issue (see above)
1. Open an MR from the issue using the "Create merge request" button. This automatically creates a unique branch based on the issue name. This marks the issue for closure once the MR is merged.
1. Push your work to the branch
1. Run any relevant jobs to the work being proposed
  * e.g. if you're working on dbt changes, run the job most appropriate for your changes. See the [dbt changes MR template checklist](https://gitlab.com/gitlab-data/analytics/blob/master/.gitlab/merge_request_templates/dbt%20Model%20Changes.md) for a list of jobs and their uses.
1. Document in the MR description what the purpose of the MR is, any additional changes that need to happen for the MR to be valid, and if it's a complicated MR, how you verified that the change works. See [this MR](https://gitlab.com/gitlab-data/analytics/merge_requests/658) for an example of good documentation. The goal is to make it easier for reviewers to understand what the MR is doing so it's as easy as possible to review.
1. Assign the MR to a peer to have it reviewed. If assigning to someone who can merge, either leave a comment asking for a review without merge, or you can simply leave the `WIP:` label.
  * Note that assigning someone an MR means action is required from them.
  * Adding someone as an approver is a way to tag them for an FYI. This is similar to doing `cc @user` in a comment.
1. Once it's ready for further review and merging, remove the `WIP:` label, mark the branch for deletion, mark squash commits, and assign to the project's maintainer. Ensure that the attached issue is appropriately labeled and pointed.

Other tips:
* Reviewers should have 48 hours to complete a review, so plan ahead with the end of the milestone.
* When possible, questions/problems should be discussed with your reviewer before MR time. MR review time is by definition the worst possible time to have to make meaningful changes to your models, because you’ve already done all of the work!

### Local Docker Workflow

To faciliate an easier workflow for analysts and to abstract away some of the complexity around handling `dbt` and its dependencies locally, the main analytics repo now supports using `dbt` from within a `Docker` container.
There are commands within the `Makefile` to facilitate this, and if at any time you have questions about the various `make` commands and what they do, just use `make help` to get a handy list of the commands and what each of them does.

Before your initial run (and whenever the containers get updated) make sure to run the following commands:

1. `make update-containers`
1. `make cleanup` 

These commands will ensure you get the newest versions of the containers and generally clean up your local `Docker` environment.

##### Using `dbt`:

- To start a `dbt` container and run commands from a shell inside of it, use `make dbt-image`.
- This will automatically import everything `dbt` needs to run, including your local `profiles.yml` and repo files.
- ~~To see the docs for your current branch, run `make dbt-docs` and then visit `localhost:8081` in a web-browser.~~ There is a quoting bug in `dbt` that prevents this from working as intended, expected fix will be in `dbt` version `0.15`
- Once inside of the `dbt` container, run any `dbt` commands as you normally would. 
- Changes that are made to any files in the repo will automatically be updated within the container. There is no need to restart the container when you change a file through your editor!

### YouTube

We encourage everyone to record videos and post to GitLab Unfiltered. The [handbook page on YouTube](/handbook/communication/youtube/#post-everything) does an excellent job of telling why we should be doing this. If you're uploading a video for the data team, be sure to do the following extra steps:

* Add `data` as a video tag
* Add it to the [Data Team playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI)
* Share the video in #data channel on slack

## <i class="fas fa-cubes fa-fw icon-color font-awesome" aria-hidden="true"></i>Our Data Stack

We use GitLab to operate and manage the analytics function.
Everything starts with an issue.
Changes are implemented via merge requests, including changes to our pipelines, extraction, loading, transformations, and parts of our analytics.

|Stage|Tool|
|:-|:-:|
|Extraction|Stitch, Fivetran, and Custom|
|Loading|Stitch, Fivetran, and Custom|
|Orchestration|Airflow|
|Storage|Snowflake|
|Transformations|dbt and Python scripts|
|Analysis| Periscope Data |

## <i class="fas fa-exchange-alt fa-fw icon-color font-awesome" aria-hidden="true"></i>Extract and Load

We currently use [Stitch](https://www.stitchdata.com) and [Fivetran](https://fivetran.com/) for most of our data sources. These are off-the-shelf ELT tools that remove the responsibility of building, maintaining, or orchestrating the movement of data from some data sources into our Snowflake data warehouse. We run a full-refresh of all of our Stitch/Fivetran data sources at the same time that we rotate our security credentials (approx every 90 days).

| Data Source       | Pipeline  | Management Responsibility | Frequency |
|-------------------|-----------|-----------------|-----------------|
| BambooHR | Airflow | Data Team | 12 hour intervals for all time |
| Clearbit |     |    |   |
| CloudSQL Postgres | Stitch    | Data Team   |   |
| Customer DB | Postgres_Pipeline   | Data Team   | |
| DiscoverOrg |     |    |   |
| Gitter            |           |           |  not updated |
| GitLab.com    | Postgres_Pipeline | Data Team  |   |
| Greenhouse | Airflow (custom script) | Data Team | Once per day |
| License DB | Postgres_Pipeline   | Data Team   | |
| Marketo           | Stitch    | Data Team   |   12 hour intervals - Backfilled from January 1, 2013|
| Netsuite          | Fivetran  | Data Team   |   6 hour intervals - Backfilled from January 1, 2013|
| Salesforce (SFDC) | Stitch    | Data Team   |   1 hour intervals - Backfilled from January 1, 2013|
| SheetLoad         | SheetLoad | Data Team   |  24 hours |
| Snowplow          | Snowpipe  | Data Team   | Continuously loaded |
| Version DB (Pings)| Postgres_Pipeline   | Data Team   | |
| Zendesk           | Stitch    | Data Team   |   1 hour intervals - Backfilled from January 1, 2013|
| Zuora             | Stitch    | Data Team   |   30 minute intervals - Backfilled from January 1, 2013|

### SLOs (Service Level Objectives) by Data Source

This is the lag between real-time and the analysis displayed in the [data visualization tool](#visualization).

| Data Source       | SLO  |
|--------------------|-----------|
| BambooHR           | 1 day |
| Clearbit           | None |
| Airflow DB         | 9 hours |
| CI Stats DB        | None - Owned by [GitLab.com Infrastructure Team](/handbook/engineering/infrastructure/), intermittently unavailable |
| Customer DB        | None - Owned by [GitLab.com Infrastructure Team](/handbook/engineering/infrastructure/), intermittently unavailable |
| DiscoverOrg        | None |
| GitLab.com         | None - Owned by [GitLab.com Infrastructure Team](/handbook/engineering/infrastructure/), intermittently unavailable |
| GitLab Profiler DB | None - Owned by [GitLab.com Infrastructure Team](/handbook/engineering/infrastructure/), intermittently unavailable |
| Greenhouse         | 2 days |
| License DB         | None - Owned by [GitLab.com Infrastructure Team](/handbook/engineering/infrastructure/), intermittently unavailable |
| Marketo            | None |
| Netsuite           | 1 day |
| Salesforce (SFDC)  | 1 day|
| SheetLoad          | 2 days |
| Snowplow           | 1 day |
| Version DB (Pings) | None - Owned by [GitLab.com Infrastructure Team](/handbook/engineering/infrastructure/), intermittently unavailable |
| Zendesk            | 1 day |
| Zuora              | 1 day |


### Adding new Data Sources and Fields

Process for adding a new data source:
* Create a new issue in the Analytics project requesting for the data source to be added:
  * Document what tables and fields are required
  * Document the questions that this data will help answer
* Create an issue in the [Security project](https://gitlab.com/gitlab-com/gl-security/engineering/issues/) and cross-link to the Analytics issue.
  * Tag the Security team `gitlab-com/gl-security`

To add new fields to the BambooHR extract:

* Create a new issue in the Analytics project using the BambooHR template
* Gain approval from a Data Team Manager and the Compensation and Benefits Manager
* Once approved, assign to the Compensation and Benefits Manager and a Data Engineer who will verify the extract

### Data Team Access to Data Sources

In order to integrate new data sources into the data warehouse, specific members of the Data team will need admin-level access to data sources, both in the UI and through the API.
We need this admin-level access through the API in order to pull all the data needed to build the appropriately analyses and through the UI to compare the results of prepared analyses to the UI.

Sensitive data sources can be limited to no less than 1 data engineer and 1 data analyst having access to build the require reporting.
In some cases, it may only be 2 data engineers.
We will likely request an additional account for the automated extraction process.

Sensitive data is locked down through the security paradigms listed below;
Periscope will never have access to sensitive data, as Periscope does not have access to any data by default.
Periscope's access is always explicitly granted.


### Using SheetLoad

SheetLoad is the process by which a Google Sheets and CSVs from GCS or S3 can be ingested into the data warehouse.

Technical documentation on usage of sheetload can be found in the [readme] in the data team project(https://gitlab.com/gitlab-data/analytics/tree/master/extract/sheetload).

If you want to import a Google Sheet or CSV into the warehouse, please [make an issue](https://gitlab.com/gitlab-data/analytics/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) in the data team project using the "CSV or GSheets Data Upload" issue template. This template has detailed instructions depending on the type of data you want to import and what you may want to do with it.

#### Things to keep in mind about SheetLoad
We strongly encourage you to consider the source of the data when you want to move it into a spreadsheet. SheetLoad should primarily be used for data whose canonical source is a spreadsheet - i.e. Sales quotas. If there is a source of this data that is not a spreadsheet you should at least make an issue to get the data pulled automatically. However, if the spreadsheet is the SSOT for this data, then we can get it into the warehouse and modelled appropriately via dbt.

We do understand, though, that there are instances where a one-off analysis is needed based on some data in a spreadsheet and that you might need to join this to some other data already in the warehouse. We offer a "Boneyard" schema where you can upload the spreadsheet and it will be available for querying within Periscope. We call it Boneyard to highlight that this data is relevant only for an *ad hoc/one off* use case and will become stale within a relatively short period of time.

SheetLoad is designed to make the table in the database a mirror image of what is in the sheet from which it is loading. Whenever SheetLoad detects a change in the source sheet it will forcefully drop the database table and recreate it in the image of the updated spreadsheet. This means that if columns are added, changed, etc. it will all be reflected in the database.

Except for where absolutely not possible, it is best that the SheetLoad sheet import from the original Google Sheet directly using the `importrange` function. This allows you to leave the upstream sheet alone and while enabling you to format the sheetload version to be plain text. Any additional data type conversions or data cleanup can happen in the base dbt models. (This does not apply to the Boneyard.)

### Snowplow Infrastructure

In June of 2019, we switched sending Snowplow events from a third party to sending them to infrastructure managed by GitLab.
From the perspective of the data team, not much changed from the third party implementation.
Events are sent through the collector and enricher and dumped to S3.
See Snowplow's [architecture overview](https://github.com/snowplow/snowplow/#snowplow-technology-101) for more detail.

Enriched events are stored in TSV format in the bucket `s3://gitlab-com-snowplow-events/output/`.
Bad events are stored as JSON in `s3://gitlab-com-snowplow-events/enriched-bad/`.
For both buckets, there are paths that follow a date format of `/YYYY/MM/DD/HH/<data>`.

For details on how the ingestion infrastructure was set up, please see the [Snowplow section](/handbook/business-ops/data-team/snowplow) of the handbook.


### Data Source Overviews
  * [Customer Success Dashboards](https://drive.google.com/open?id=1FsgvELNmQ0ADEC1hFEKhWNA1OnH-INOJ)
  * [Netsuite](https://www.youtube.com/watch?v=u2329sQrWDY)
    * [Netsuite and Campaign Data](https://drive.google.com/open?id=1KUMa8zICI9_jQDqdyN7mGSWSLdw97h5-)
  * [Pings](https://drive.google.com/file/d/1S8lNyMdC3oXfCdWhY69Lx-tUVdL9SPFe/view)
  * [Salesforce](https://youtu.be/KwG3ylzWWWo)
  * [Zendesk](https://drive.google.com/open?id=1oExE1ZM5IkXcq1hJIPouxlXSiafhRRua)

## <i class="fas fa-clock fa-fw icon-color font-awesome" aria-hidden="true"></i> Orchestration

We use Airflow on Kubernetes for our orchestration. Our specific setup/implementation can be found [here](https://gitlab.com/gitlab-data/data-image).

## <i class="fas fa-database fa-fw icon-color font-awesome" aria-hidden="true"></i> Data Warehouse

We currently use [Snowflake](https://docs.snowflake.net/manuals/index.html) as our data warehouse.

### Warehouse Access

To gain access to the data warehouse:
* Create an issue in the [access requests project](https://gitlab.com/gitlab-com/access-requests) documenting the level of access required.
* Do not request a shared account - each account must be tied to a user.
* We loosely follow the paradigm explained in [this blog post](https://blog.fishtownanalytics.com/how-we-configure-snowflake-fc13f1eb36c4) around permissioning users.

#### Snowflake Permissions Paradigm

Goal: Mitigate risk of people having access to sensitive data.

We currently use [Meltano](https://meltano.com/)'s Permission Bot in dry mode to help manage our user, roles, and permissions for Snowflake.
Documentation on the permission bot is in [the Meltano docs](https://meltano.com/docs/meltano-cli.html#meltano-permissions). Our configuration file for our Snowflake instance is stored in [this roles.yml file](https://gitlab.com/gitlab-data/analytics/blob/master/load/snowflake/roles.yml).

There are four things that we need to manage:
* databases - who has access to which ones
* roles
* user roles (mapped directly to users)
* warehouses

There are currently four primary analyst roles in Snowflake:
* analyst_growth
* analyst_finance
* analyst_core
* analyst_sensitive

Analysts are assigned to relevant roles and are explicitly granted access to the schemas they need.

Two notes of the permission bot:
* things that exist but aren't in the file don't lead to errors
* it does not _delete_ things (or produce those commands) when removing from the file

Common errors that are encountered:
* If there is a role referenced but not defined then it will error and the solution is to properly fix the config file.
* If a role is defined in the spec but does not exist in Snowflake then it will error. The solution is to create the role in Snowflake.

#### Data Storage

We currently use two databases- `raw` and `analytics`. The former is for EL'ed data; the latter is for data that is ready for analysis (or getting there).

All database clones are wiped away every week.

##### Raw

* Raw may contain sensitive data, so permissions need to be carefully controlled.
* Data is stored in different schemas based on the source.
* User access can be controlled by schema and tables.

##### Analytics

* Analytics has three "prod" schemas: `analytics`, `analytics_staging`, and `analytics_sensitive`.
  * By default, all dbt models are in the `analytics` schema.
* Analysts' dev schemas (`jdoe_scratch_analytics` and `jdoe_scratch_staging`) are also in the analytics database. The dev schemas could have sensitive information, as they iterate through it. These are not accessible within the GitLab Periscope workspace.
* All dev schemas are wiped away every week.
* If a new model is added to the `analytics_sensitive` schema, it must also be added to any relevant roles in the roles.yml file detailed above.


<div class="panel panel-success">
**Managing Roles for Snowflake**
{: .panel-heading}
<div class="panel-body">

Here are the proper steps for provisioning a new user and user role:

* Login and switch to `securityadmin` role
* Create user
  * User name: `JSMITH` - This is the GitLab default of first letter of first name and full last name.
  * Do not create a password for the user as sign-on is controlled via Okta
  * Click next and fill in additional info.
    * Make Login Name their email. This should match the user name just with @gitlab.com appended.
    * Display name should match match user name (all caps).
    * First and Last name can be normal.
  * Do not set any defaults
* Create role for user (`JSMITH`) with `sysadmin` as the parent role (this grants the role to sysadmin)
* Grant user role to new user
* Grant any additional roles to user
* Add [future grant](https://docs.snowflake.net/manuals/sql-reference/sql/grant-privilege.html) to `analytics` and `analytics_staging` schemas to user with `grant select on future tables in schema <schema> to role <username>` using the `sysadmin` role
* Document in Snowflake config.yml permissions file
* Ensure the user is assigned the application in Okta 
</div>
</div>

#### Snowflake Compute Resources

Compute resources in Snowflake are known as "warehouses".
To better track and monitor our credit consumption, we have created several warehouses depending on who is accessing the warehouse.
The names of the warehouse are appended with their size (`analyst_s` for small)

* `admin` - This is for permission bot and other admin tasks
* `airflow_testing_l` - For testing Airflow locally
* `analyst_*` - These are for Data Analysts to use when querying the database or modeling data
* `engineer_*` - These are for Data Engineers and the Manager to use when querying the database or modeling data
* `fivetran_warehouse` - This is exclusively for Fivetran to use
* `gitlab_postgres` - This is for extraction jobs that pull from GitLab internal Postgres databases
* `loading` - This is for our Extract and Load jobs
* `merge_request_*` - These are scoped to GitLab CI for dbt jobs within a merge request
* `reporting` - This is for the BI tool for querying
* `stitch` - This is exclusively for Stitch to use
* `target_snowflake` - This is for the Meltano team to test their Snowflake loader
* `transforming_*` - These are for production dbt jobs

#### Timezones

All timestamp data in the warehouse should be stored in UTC. The [default timezone](https://docs.snowflake.net/manuals/sql-reference/parameters.html#timezone) for a Snowflake sessions is PT, but we have overridden this so that UTC is the default. This means that when `current_timestamp()` is queried, the result is returned in UTC.

[Stitch explicitly converts](https://www.stitchdata.com/docs/data-structure/snowflake-data-loading-behavior#%0A%0A%09%0A%09%09%09%09%09a-column-contains-timestamp-data%0A%0A%09%09%09%09%0A%0A%0A) timestamps to UTC. Fivetran does this as well (confirmed via support chat).

## <i class="fas fa-cogs fa-fw icon-color font-awesome" aria-hidden="true"></i>Transformation

Please see the [data analyst onboarding issue template](https://gitlab.com/gitlab-data/analytics/blob/master/.gitlab/issue_templates/data_onboarding.md) for details on getting started with dbt.

At times, we rely on dbt packages for some data transformation.
[Package management](https://docs.getdbt.com/docs/package-management) is built-in to dbt.
A full list of packages available are on the [dbt Hub site](https://hub.getdbt.com).

### Tips and Tricks about Working with dbt
* The goal of a (final) `_xf` dbt model should be a `BEAM*` table, which means it follows the business event analysis & model structure and answers the who, what, where, when, how many, why, and how question combinations that measure the business.
* Model names should be as obvious as possible and should use full words where possible, e.g. `accounts` instead of `accts`.
* Documenting and testing new data models is a part of the process of creating them. A new dbt model is not complete without tests and documentation.
* We use a variable to reference the database in dbt base models so that if we're testing changes in a Snowflake clone, the reference can be programmatically set.
* Read [dbt's model selection syntax documentation](https://docs.getdbt.com/docs/model-selection-syntax) about specifying models during a `run` or `test`. Specifying models can save you a lot of time by only running/testing the models that you think are relevant. However, there is a risk that you'll forget to specify an important upstream dependency so it's a good idea to understand the syntax thoroughly. 
* Definitions to know
   * `dbt seed` - a command that loads data from csv files into our data warehouse. Because these csv files are located in our dbt repository, they are version controlled and code reviewable. Thus, dbt seed is appropriate for loading static data which changes infrequently. A csv file that’s up to ~1k lines long and less than a few kilobytes is probably a good candidate for use with the `dbt seed` command.
   * `source table` - (can also be called `raw table`) table coming directly from data source as configured by the manifest. It is stored directly in a schema that indicates its original data source, e.g. `sfdc`
   * `base models`- the only dbt models that reference the source table; base models have minimal transformational logic (usually limited to filtering out rows with data integrity issues or actively flagged not for analysis and renaming columns for easier analysis); can be found in the `analytics_staging` schema; is used in `ref` statements by `end-user models`
   * `end-user models` - dbt models used for analysis. The final version of a model will likely be indicated with an `_xf` suffix when it’s goal is to be a `BEAM*` table. It should follow the business event analysis & model structure and answer the who, what, where, when, how many, why, and how question combinations that measure the business. End user models are found in the `analytics` schema.

Schema References (aka What goes where)

| Purpose | Production | Dev | Config |
|---|---|---|---|
| For querying & analysis               | analytics                   | emilie_scratch_analytics                   | None |
| For querying & analysis staging       | analytics_staging           | emilie_scratch_staging                     | {{  config({ "schema": "staging"}) }} |
| For querying & analysis but SENSITIVE | analytics_sensitive         | emilie_scratch_analytics_sensitive         | {{  config({ "schema": "sensitive"}) }} |

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/06101b50-9610-48e4-b1bd-2200eb488f41?embed=true" height="400"> </iframe>

#### Snapshots

##### Create snapshot tables with `dbt snapshot`

Snapshots are a way to take point-in-time copies of source tables. dbt has [excellent documentation](https://docs.getdbt.com/docs/snapshots) on how the snapshots work. Snapshots are stored in the [snapshots folder](https://gitlab.com/gitlab-data/analytics/tree/master/transform/snowflake-dbt/snapshots) of our dbt project. We have organised the different snapshots by data source for easy discovery.

The following is an example of how we implement a snapshot:

```sql
{% snapshot sfdc_opportunity_snapshots %}

    {{
        config(
          target_database='RAW',
          target_schema='snapshots',
          unique_key='id',
          strategy='timestamp',
          updated_at='systemmodstamp',
        )
    }}
    
    SELECT * 
    FROM {{ source('salesforce', 'opportunity') }}
    
{% endsnapshot %}
```

Key items to note:
* The `RAW` database is hard-coded. We aim to set this as an environment variable pending some upstream bugs [gitlab-data/analytics/2299](https://gitlab.com/gitlab-data/analytics/issues/2299)
* The  `snapshots` schema is hard-coded and should not change
* _Always_ select from a source table. Even if some deduplication is required, a source table must be selected from as selecting from a downstream dbt model is prone to failure
* As snapshots are stored in raw, your role will need to be explicitly granted access to the schema or tables
* Follow the naming convention `{source_name}_{source_table_name}_snapshots` for your file name 
* Avoid any transformations in snapshots aside from deduplication efforts. Always clean data downstream
* Unless you don't have a reliable `updated_at` field, always prefer using `timestamp` as a strategy (over `check`). Please find [documentation about strategy here](https://docs.getdbt.com/docs/snapshots#section-how-does-dbt-know-which-rows-have-changed-)

Snapshots are tested manually by a maintainer of the Data Team project before merging.

##### Make snapshots table available in analytics data warehouse

As stated above, `RAW` database and `snapshots` schemas are hard-coded in the config dictionary of dbt snapshots. That means, once these snapshot tables are created in the `RAW` we need to make them available in the `ANALYTICS` data warehouse in order to be able to query them downstream (with Periscope or for further `_xf` dbt models).

Base models for snapshots are available in the folder /models/snapshots of our dbt project. 
Key items to note:

* Before writing a snapshot base model, don't forget to add it in the [sources.yml file](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/models/snapshots/sources.yml) (keep this file sorted)
* The name of the table in the data warehouse should be consistent with our data warehouse design guideline. Ideally we would like to stick to `{source_name}_{source_table_name}_snapshots` as our naming convention. But dbt doesn't like when it finds duplicated file names in its projects. In order to avoid this scenario (the snapshot and the snapshot base model having the same name), we found this clean and simple workaround:
  * The name of the base model file will be the name of the source snapshot table to which we suffix `_base`. Ex: we have a `gitlab_dotcom_members_snapshots` snapshot file [here](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/snapshots/gitlab_dotcom_members_snapshots.sql) and a base model of this snapshot [here](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/models/snapshots/gitlab_dotcom_members_snapshots_base.sql) named `gitlab_dotcom_members_snapshots_base`. 
  * We use the [dbt config alias argument](https://docs.getdbt.com/docs/using-custom-aliases#section-usage) to rename the table by removing the `_base` suffix and keep the table name clean
* If a base model built upon the snapshotted source table exists, please re-use the query that has been already written and apply the following modifications:
  * Remove the deduplication process, it is not necessary.
  * Always add `dbt_scd_id` as a primary key to your snapshot base model and rename it to something more explicit (documentation about snapshot meta-fields can be found [here](https://docs.getdbt.com/docs/snapshots#section-snapshot-meta-fields))
  * Add columns `dbt_valid_from` and `dbt_valid_to` to your query
  * Good example [here with the snapshot base model](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/models/snapshots/gitlab_dotcom_gitlab_subscriptions_snapshots_base.sql) and [the base model](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/models/gitlab_dotcom/base/gitlab_dotcom_gitlab_subscriptions.sql)

### Configuration for dbt
* In the `~/.dbt/` folder there should be a `profiles.yml`file that looks like this [sample profile](https://gitlab.com/gitlab-data/analytics/blob/master/admin/sample_profiles.yml)
* The smallest possible warehouse should be stored as an evironment variable. Our dbt jobs use `SNOWFLAKE_TRANSFORM_WAREHOUSE` as the variable name to identify the warehouse. The environment variable can be set in the `.bashrc` or `.zshrc` file as follows:
  * `export SNOWFLAKE_TRANSFORM_WAREHOUSE="ANALYST_XS"`
  * In cases where more compute is required, the variable can be overwritten by adding `--vars '{warehouse_name: analyst_xl}'` to the dbt command


## <i class="fas fa-chart-bar fa-fw icon-color font-awesome" aria-hidden="true"></i>Visualization

We use [Periscope](https://www.periscopedata.com) as our Data Visualization and Business Intelligence tool. To request access, please follow submit an [access request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request).

#### Meta Analyses for the Data Team

* [Periscope Usage! 📈](https://app.periscopedata.com/app/gitlab/410320/)
* [Periscope Account Optimization 💪](https://app.periscopedata.com/app/gitlab/410321/)
* [Periscope Account Maintenance 🗑️](https://app.periscopedata.com/app/gitlab/410322/)
* [dbt Event Logging](https://app.periscopedata.com/app/gitlab/420622/)
* [Snowflake Spend ️❄](https://app.periscopedata.com/app/gitlab/443349/)

## <i class="fas fa-user-lock fa-fw icon-color font-awesome" aria-hidden="true"></i>Security

### Passwords

Per GitLab's password policy, we rotate service accounts that authenticate only via passwords every 90 days. A record of systems changed and where those passwords were updated is kept in [this Google Sheet](https://docs.google.com/spreadsheets/d/17T89cBIDLkMUa3rIw1GxS-QWFL7kjeLj2rCQGZLEpyA/edit?usp=sharing).

## Data Learning and Resources

### Recommended Reading, Listening, Watching

* [How Data Teams Do More With Less By Adopting Software Engineering Best Practices - Thomas's talk at the 2018 DataEngConf in NYC](https://www.youtube.com/watch?v=eu623QBwakc)
  * [Slides from a similar talk by Taylor at the 2019 Music City Tech Conference in Nashville](https://docs.google.com/presentation/d/1oSdej0y7o5d4DKlyykmTEMbokr7sRIxMkFmw6Z8a270/edit?usp=sharing)
* [Locally Optimistic AMA: August 2019 with Taylor Murphy](https://www.youtube.com/watch?v=iT1uRdyXfd8)
* [Deploying your first dbt project with GitLab CI](https://www.youtube.com/watch?v=-XBIIY2pFpc&feature=youtu.be&t=1305)
* [DataOps in a Cloud Native World](https://www.youtube.com/watch?v=PLe9sovhtGA&list=PLFGfElNsQthaaqEAb6ceZvYnZgzSM50Kg&index=9&t=0s)
* [GitLab blog post about Meltano](https://news.ycombinator.com/item?id=17667399) 
  * [Livestream chat with Sid and HN user slap_shot](https://www.youtube.com/watch?v=F8tEDq3K_pE)
  * [Follow-up blog post to original Meltano post](/blog/2018/08/07/meltano-follow-up/)
* [The AI Hierarchy of Needs](https://hackernoon.com/the-ai-hierarchy-of-needs-18f111fcc007)
* [Data Meta Metrics](https://caitlinhudon.com/2017/11/14/data-meta-metrics/)
* [Engineers Shouldn’t Write ETL](https://multithreaded.stitchfix.com/blog/2016/03/16/engineers-shouldnt-write-etl/)
* [The Startup Founder’s Guide to Analytics](https://thinkgrowth.org/the-startup-founders-guide-to-analytics-1d2176f20ac1)
* [Functional Data Engineering — a modern paradigm for batch data processing](https://medium.com/@maximebeauchemin/functional-data-engineering-a-modern-paradigm-for-batch-data-processing-2327ec32c42a)
* [Keep it SQL Stupid, a talk by Connor McArthur of Fishtown Analytics at DataEngConf SF '18 explaining dbt](https://www.youtube.com/watch?v=9VNh11qSfAo)
* [Views on Vue Podcast with Jacob Schatz and Taylor Murphy](https://devchat.tv/views-on-vue/vov-030-how-we-use-vue-in-data-science-with-jacob-schatz-taylor-murphy-gitlab-team/)
* [Pain Points by the Airflow Podcast with @tlapiana](https://soundcloud.com/the-airflow-podcast/pain-points)
* [DevOps for AI](https://www.youtube.com/watch?v=HwZlGQuCTj4)
* [What Can Data Scientists Learn from DevOps](https://redmonk.com/dberkholz/2012/11/06/what-can-data-scientists-learn-from-devops/)
* [One Analyst's Guide for Going from Good to Great](https://blog.fishtownanalytics.com/one-analysts-guide-for-going-from-good-to-great-6697e67e37d9)
* The Value of Data: [Part 1](https://www.codingvc.com/the-value-of-data-part-1-using-data-as-a-competitive-advantage), [Part 2](https://www.codingvc.com/the-value-of-data-part-2-building-valuable-datasets), [Part 3](https://www.codingvc.com/the-value-of-data-part-3-data-business-models)
* [Building a Data Practice](https://www.locallyoptimistic.com/post/building-a-data-practice/)
* [Does my Startup Data Team Need a Data Engineer](https://blog.fishtownanalytics.com/does-my-startup-data-team-need-a-data-engineer-b6f4d68d7da9)
* [Data Science is different now](https://veekaybee.github.io/2019/02/13/data-science-is-different/) (Note: this is why GitLab doesn't have a Data Scientist yet.)
* [Why You Don't Need Data Scientists](https://medium.com/@kurtcagle/why-you-dont-need-data-scientists-a9654cc9f0e4)
* [Resources Written by dbt Community Members](https://discourse.getdbt.com/t/resources-written-by-community-members/276)

### Data Newsletters
- [The Carpentries](https://carpentries.topicbox.com/latest)
- [Data Science Roundup Newsletter](http://roundup.fishtownanalytics.com/)
- [Data is Plural](https://tinyletter.com/data-is-plural)
- [DataEng Weekly](http://dataengweekly.com/)
- [Data Elixir](https://dataelixir.com/)
- [Data Science Weekly](https://www.datascienceweekly.org/)
- [Lantrns Analytics (Product Analytics)](https://www.lantrns.co/product-analytics-newsletter/)
- [Music and Tech](https://angelddaz.substack.com)
- [Normcore Tech](https://vicki.substack.com)
- [One Shot Learning](https://buttondown.email/oneshotlearning)
- [SF Data](http://weekly.sfdata.io/)


### Data Blogs
- [Airbnb](https://airbnb.io)
- [Ask Good Questions](https://askgoodquestions.blog/)
- [Buffer Blog](https://data.buffer.com)
- [Calogica](https://calogica.com/blog)
- [Fishtown Analytics Blog](https://blog.fishtownanalytics.com)
- [Go Data Driven](https://blog.godatadriven.com)
- [MBA Mondays](https://avc.com/archive/#mba_mondays_archive)
- [Mode Analytics Blog](https://blog.modeanalytics.com/)
- [Multithreaded](https://multithreaded.stitchfix.com/)
- [Periscope Data Blog](https://www.periscopedata.com/blog)
- [Silota](http://www.silota.com/docs/recipes/)
- [Wes McKinney Blog](http://wesmckinney.com/archives.html)
- [Data Ops](https://medium.com/data-ops)
- [Retina AI Blog](https://retina.ai/blog/)
- [StitchFix Algorithms Blog](https://multithreaded.stitchfix.com/algorithms/blog/)
- [Five Thirty Eight](https://data.fivethirtyeight.com/)
- [Data.gov](https://www.data.gov/)

### Data Visualization Resources
- [Storytelling with Data](http://storytellingwithdata.com/)
- [Data Revelations](https://www.datarevelations.com/)
- [Eager Eyes](https://eagereyes.org/)
- [FiveThirtyEight's DataLab](https://fivethirtyeight.com/features/)
- [Flowing Data](https://flowingdata.com/)
- [From Data to Viz](https://www.data-to-viz.com/)
- [Gravy Anecdote](http://gravyanecdote.com/)
- [JunkCharts](https://junkcharts.typepad.com/)
- [Make a Powerful Point](http://makeapowerfulpoint.com/)
- [Makeover Monday](http://www.makeovermonday.co.uk)
- [Perceptual Edge](https://perceptualedge.com/)
- [PolicyViz](https://policyviz.com/)
- [The Functional Art](http://www.thefunctionalart.com/)
- [The Pudding](https://pudding.cool/)
- [Visualising Data](http://www.visualisingdata.com/)
- [VizWiz](http://www.vizwiz.com/)
- [WTF Visualizations](http://viz.wtf/)

### Data Slack Communities
- [Data Viz Society](https://datavizsociety.slack.com)
- [Data Science Community](https://dscommunity.slack.com)
- [dbt](https://slack.getdbt.com)
- [Locally Optimistic](https://www.locallyoptimistic.com/community/)
- [Measure](ttps://docs.google.com/forms/d/e/1FAIpQLSdyAxOcI8z1EEiJDW4sGln-1GK9eJV8Y86eljX-uSlole0Vtg/viewform?c=0&w=1)
- [Meltano](https://join.slack.com/t/meltano/shared_invite/enQtNTM2NjEzNDY2MDgyLWI1N2EyZjA1N2FiNDBlNDE2OTg4YmI1N2I3OWVjOWI2MzIyYmJmMDQwMTY2MmUwZjNkMTBiYzhiZTI2M2IxMDc)
- [Open Data Community](opendatacommunity.slack.com)
- [Pachyderm](http://slack.pachyderm.io)
- [PyCarolinas](bit.ly/2Y8aoIp)
- [R for Data Analysis](r-data-team.slack.com)
- [Software Engineering Daily](http://softwaredaily.herokuapp.com)
- [The Data School](https://thedataschool.slack.com)

### Technical Learning Resources
- [Chris Albon](https://chrisalbon.com/#postgresql)
- [Mode SQL Tutorial](https://mode.com/sql-tutorial/introduction-to-sql/)
- [Machine Learning Resources](https://drive.google.com/drive/folders/1sOXWW-FujwKU2T-auG7KPR9h6xqDRx0z?usp=sharing) (GitLab Internal)
- [Codecademy](https://www.codecademy.com/learn/learn-sql)
- [DataQuest](https://www.dataquest.io/course/sql-fundamentals/)
- [Khan Academy](https://www.khanacademy.org/computing/computer-programming/sql)
- [HackerRank (Exercises)](https://www.hackerrank.com/domains/sql?filters%5Bstatus%5D%5B%5D=unsolved&badge_type=sql)
- [Udacity](https://www.udacity.com/course/intro-to-relational-databases--ud197)
- [Stanford University Mini-Courses](https://lagunita.stanford.edu/courses/DB/2014/SelfPaced/about)
- [The Data School by Chartio](https://dataschool.com/learn/inroduction-to-teaching-others-sql)
- [W3Schools](https://www.w3schools.com/sql/default.asp)

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i>Team Roles

### Triager

The data team has a triager who is responsible for addressing `dbt run` and `dbt test` failures;
labeling, prioritizing, and asking initial questions on created issues;
and guiding and responding to questions from GitLabbers outside of the Data Team.

Having a dedicated triager on the team helps address the bystander affect.
This is **not** an on-call position.
This role just names a clear owner.
Through clear ownership, we create room for everyone else on the team to spend most of the day around deep work.
The triager is encouraged to plan their day for the kind of work that can be accomplished successfully with this additional demand on time.

**The triager is not expected to know the answer to all the questions.
They should pull in other team members who have more knowledge in the subject matter area to pass on the conversation after understanding the issue.**
Document any issues you stumble upon and learn about to help disseminate knowledge amongst all team members.

#### Slack channels to monitor for triaging

* #data: this channel is where GitLabbers outside of the data team submit questions and inquiries. Questions often pertain to Periscope dashboards, technical questions around using SQL and Periscope, or general requests for analyses and reports.
   * Most of the requests that come in the #data channel can be solved by:
       * pointing folks to an appropriate dashboard; or
       * guiding them to create an issue in the right place.
   * It's important that folks feel acknowledged and that their questions are being addressed by the data team.
* #data-triage: this channel is where issues submitted from GitLabbers outside of the Data Team are triaged.
   * **Issue triaging**
Many issues that come into the data team project from other GitLab team members need additional info and/or context in order to be understood,
estimated, and prioritized.
It is the triager's priority to ask those questions.
This will help folks feel like their asks are not just going to the void and are being taken seriously by the data team.
It also help surface issues sooner, rather than later.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Hu_zDh0Av7M" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


* #dbt-runs: this channel is where `dbt run` and `dbt test` failures are triaged.
   * **dbt run and test failures**
Occasionally, `dbt run` and `dbt test`  produce failures.
Many of the these failures are due to upstream data quality problems that need to be addressed.
     * `dbt run` failures indicate the model did not run and maintained its previous state and data composition. These failures are not common and it is not feasible to maintain a list of common `dbt run` failures. Please follow the general checklist located at the top of the below referenced README file to debug `dbt run` errors. 
     * `dbt test` failures indicate the model ran, but there are some records in the model that have data quality issues and require research and follow-up. Please follow the checklists in the below referenced README file to debug `dbt test` failures. Some tests have a severity setting of `warn` and not `fail`. If `warn` is configured, then dbt will log a warning for any failing tests, but the job will still be pass (assuming no failures). This configuration is useful for tests in which a failure does not imply that action is required. Warnings do not need to be triaged.
   * Most `dbt test` failures are documented in the [dbt tests README](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/tests/README.md).
If you stumble upon one that isn't documented, be sure to document it to make it easier to address in the future.
The end result is not necessarily the resolution of the failing model in `dbt run` and `dbt test`; 
**however, the model should be taken to a `running` state and the problems documented in an issue.**
This is important because:
      1. Airflow only shows the last 50 lines of logs in Slack and we don't want to risk missing another issue; and 
      2. We reduce the noise for all team members this way, while things are actively worked to be resolved. 
If you see that there are multiple errors, you may need to ping someone with Airflow access to get the full logs. 
   * When creating an issue from a pipeline failure, add the `triage` label so that the next person on triage duty can easily find if the failure is new or not, and avoid issue duplication. It should also be posted in the #dbt-runs channel on Slack, as that channel is the primary place for communication around dbt events.


**Triage schedule**
The data team has implemented a triagle schedule that takes advantage of folks native timezones. The [Data Team's Google Calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9kN2RsNDU3ZnJyOHA1OHBuM2s2M2VidW84b0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) is the source of truth for the triage schedule.
A team member who is off, on vacation, or working on a high priority project is responsible for finding coverage and communicating to the team who is taking over their coverage;
this should be updated on the [Data Team's Google Calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9kN2RsNDU3ZnJyOHA1OHBuM2s2M2VidW84b0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t).

| UTC day | Team member |
| ------ | ------ |
| Monday | @derekatwood / @kathleentam | 
| Tuesday | @jjstark |
| Wednesday | @mpeychet / @kathleentam |
| Thursday | @iweeks / @kathleentam |
| Friday | @eli_kastelein / @kathleentam  |

### Reviewer 
All GitLab data team members can, and are encouraged to, perform code review on merge requests of colleagues and community contributors. 
If you want to review merge requests, you can wait until someone assigns you one, but you are also more than welcome to browse the list of open merge requests and leave any feedback or questions you may have.

Note that while all team members can review all merge requests, the ability to *accept* merge requests is restricted to maintainers.

### Maintainer
A maintainer in any of the data team projects is not synonymous with any job title. 
Here, the data team takes from the precedent set forward by the engineering division on [the responsibilities of a maintainer](/handbook/engineering/workflow/code-review/#maintainer). 
Every data team project has at least one maintainer, but most have multiple, and some projects (like Analytics) have separate maintainers for dbt and orchestration.

#### How to become a data team maintainer 

We have guidelines for maintainership, but no concrete rules.
Maintainers should have an advanced understanding of the GitLab Data projects codebases. 
Prior to applying for maintainership, a person should get a good feel for the codebase, expertise in one or more domains, and deep understanding of our coding standards.

Apart from that, someone can be considered as a maintainer when both:

1. The MRs they've reviewed consistently make it through maintainer review without significant additionally required changes.
2. The MRs they've written consistently make it through reviewer and maintainer review without significant required changes.

Once those are done, they should:

1. Create a MR to add the maintainership to their team page entry.
2. Explain in the MR body why they are ready to take on that responsibility.
3. Use specific examples of recent "maintainer-level" reviews that they have performed.
4. The MRs should not reflect only small changes to the code base, but also architectural ones and ones that create a fully functioning addition.
5. Assign the MR to their manager and mention the existing maintainers of the relevant project (Infrastructure, Analytics, etc) and area (dbt, Airflow, etc.).
6. If the existing maintainers of the relevant group e.g., dbt, do not have significant objections, and if at least half of them agree that the reviewer is indeed ready, we've got ourselves a new maintainer!

### Job Descriptions
#### Data Analyst
[Job Family](/job-families/finance/data-analyst/){:.btn .btn-purple}

#### Data Engineer
[Job Family](/job-families/finance/data-engineer/){:.btn .btn-purple}

#### Manager
[Job Family](/job-families/finance/manager-data){:.btn .btn-purple}

#### Director of Data and Analytics
[Job Family](/job-families/finance/dir-data-and-analytics){:.btn .btn-purple}


<!-- EXTRA STYLES APPLIED FOR THIS PAGE ONLY -->

<style>
.purple {
  color: rgb(107,79,187) !important;
}
.orange {
  color:rgb(252,109,38) !important;
}
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
.btn-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-purple:hover {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv:hover {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-orange {
  color: rgb(252,109,38);
  background-color: #fff;
  border-color: rgb(226,67,41);
}
.btn-orange:hover {
  color: #fff;
  background-color: rgb(252,109,38);
  border-color: rgb(226,67,41);
}
.product.thumbnail img {
  display: block;
  max-width: 50%;
  margin: 20px auto;
}
.thumbnail img {
  display: block;
  max-width: 30%;
  margin: 20px auto;
}
.caption h4 {
  text-align: center;
}
.mkt-box {
  padding-bottom: 10px;
  padding-top: 10px;
  cursor: pointer;
}
.mkt-box:hover {
  /*border-radius: 5px;*/
  box-shadow:0 1px 5px rgba(0,0,0,0.3), 0 0 2px rgba(0,0,0,0.1) inset;
}
.mkt-row {
  padding-top: 20px;
  padding-bottom: 5px;
}
.mkt-row a:focus {
  outline: none;
}
.modal-header h2 {
  margin-top: 0;
}
.modal-footer p {
  margin-bottom: 0;
}
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.description {
  color: #999;
}
.extra-space {
  margin-bottom: 5px;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
ul.toc-list-icons li ul li i.slack {
  color: rgb(224,23,101);
}
ul.toc-list-icons li ul li i.email {
  color: rgb(192,0,0);
}
</style>
