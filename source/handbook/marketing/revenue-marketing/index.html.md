---
layout: handbook-page-toc
title: "Revenue Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Other Pages Related to Revenue Marketing

- [Business Operations](/handbook/business-ops)
     - [Resources](/handbook/business-ops/resources/)
- [Reseller Handbook](/handbook/resellers/)

## What is Revenue Marketing Handbook

The Revenue Marketing department includes the Sales Development, Field Marketing and Digital Marketing Programs (including Online Marketing & Marketing Program Management). The units in this functional group employ a variety of marketing and sales crafts in service of our current and future customers with the belief that providing our audiences value will in turn grow GitLab's business.

## Revenue Marketing Handbooks

- [Sales Development](/handbook/marketing/revenue-marketing/xdr/)
- [Field Marketing](/handbook/marketing/revenue-marketing/field-marketing/)
- [Digital Marketing Management](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/)
- [Marketing Programs Management](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/)

## Revenue Marketing KPI Definitions 
- Many of these can be found on the [Marketing Metric Dashboard](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics)

- **Total # of MQLs by month**: Total number of [Marketo Qualified Leads](/handbook/business-ops/resources/#sts=MQL%20Definition) per month
<embed width="100%" height="100%" src="https://www.periscopedata.com/api/embedded_dashboard?data=%7B%22chart%22%3A+6592066%2C+%22dashboard%22%3A+431555%2C+%22embed%22%3A+%22v2%22%2C+%22border%22%3A+%22off%22%7D&signature=7d5f5c2418d0849bdd30612fe0859ce3cb27315bfc3c93c9fc8e04684466f456">

- **Net New Business Pipeline Created ($s)**: The combined IACV of the net "New Business" opportunities
<embed width="100%" height="100%" src="https://www.periscopedata.com/api/embedded_dashboard?data=%7B%22chart%22%3A+6592070%2C+%22dashboard%22%3A+431555%2C+%22embed%22%3A+%22v2%22%2C+%22border%22%3A+%22off%22%7D&signature=8c3f86b3091c59f118dcb7bc03f27e2a3e28fa177d454a7f2adee0e4f8dfbaf5">

- **Lead follow-up response time**: The amount of time (Days, hours, minutes) it takes for a Lead/Contact to move from MQL to another status - this indicates how long it took for a sales rep/xDR to respond to the record becoming a MQL.
- **Opportunities per XDR per month created**: The net number of unique opportunities that are created and have either a "business development representative" or "sales development representative" assigned/attached to the opportunity segmented by xDR and by month. 
- **Pipe-to-spend per marketing activity**: Pipeline (IACV on Opportunities) divided by the total marketing spend, broken down by marketing activity. 
- **Marketing Efficiency Ratio**: Marketing efficiency for a given month is calculated as the ratio of IACV from closed won opportunities for the current month and the 2 preceding months vs the Marketing Operating Expenses (IACV / Marketing Operating Expenses) for the same time period. GitLab's target is greater than 2.
