---
layout: markdown_page
title: "GitLab Announces Schedule for 2019 GitLab Commit London"
---

_Inaugural user event will feature talks by Capgemini UK, Ocado Technology, Porsche AG, Siemens AG, and more showcasing the power of DevOps in action_

**LONDON, ENGLAND — August 29, 2019 —** Today [GitLab](https://about.gitlab.com/), the DevOps platform delivered as a single application, announced initial programming and speakers for [2019 GitLab Commit London](https://about.gitlab.com/events/commit/?utm_medium=pressrelease&utm_campaign=commitlondon), taking place October 9 in London.

GitLab Commit, GitLab's inaugural user event, will bring together developers, operations and security professionals to share DevOps best practices and lessons learned. Speakers will showcase the power of DevOps in action through strategy and technology discussions, lessons learned, behind-the-scenes looks at the development lifecycle, and more. 

“GitLab has over 100,000 companies and millions of developers using and co-creating on the project with us, so we tailored GitLab Commit with the end-user in mind. The event will dive into how the DevOps transformation is occurring in enterprises on an organizational level and how one can go back to their organization and implement similar practices,” said Priyanka Sharma, Director of Technical Evangelism at GitLab. “Commit invokes action and we hope attendees will commit to building the best devops organizations.”

**Conference Sessions and Speakers**
 
The conference agenda features a mix of topics, including technical sessions, live coding, deep-dives, and case studies covering cloud native, Kubernetes, CI/CD, Data, open source, security, all-remote, and more.
 
“I’ve learnt so much from the community over the past year, to be able to share some of my own work in the hope that it inspires others and drives the next chapter of my adventure is such a wonderful opportunity.” - Tom West, Solutions Architect at Converging Data EMEA 
 
The carefully-curated schedule will feature sessions from flagship GitLab customers, cloud native experts, and well-known open source communities, including:

* “Keynote: Establishing a DevOps Culture @ Porsche - A GitLab Success Story” – Dennis Menge and Alberto Gisbert, Porsche AG
* “Keynote: Flying From Base to Native Within the Clouds” – Alexandru Constantin Viscreanu, kiwi.com
* “Panel Discussion: How Ginormous Orgs Leverage GitLab”
* “Panel Discussion: Closing the SDLC loop - a Security Panel”
* “How we scaled from 10 to 30000 developers” –  Roger Meier, Siemens
* “Four years with GitLab in Ocado Technology” – Piotr Kurpik and Artur Frysiak, Ocado Technology
* “Producing a single DevOps view using data tools” –  Tom West, Converging Data EMEA
* “From Zero to Production with Rust, Python and GitLab CI” –  Mario García, Mozilla
* “The Beauty of GitLab CI/CD” –  Getty Orawo, Podii
* “GitLab All the Way Down: A Small Startup's Tale of Growing with GitLab” – Huss EL-Sheikh, 9fin
* “Continuous Verification: The missing link to fully automate your pipeline” –  Bill Shetti and Sean O'Dell, VMware
* “Evolve into a DevOps butterfly” – Matteo Codogno and Simone D'avico, wellD
* “Zero-cost infrastructure and automatic deployments for small teams” – Niki Kontoe and George Tsiolis, Ubitech
* “What not to do while using GitLab” – Boris Waguia, Adorsys GmbH & Co. KG
* “Zero to K8s: As Fast As Possible” – Matt Smith, Capgemini UK
* “How GitLab helped a small team move fast” – Adrian Moisey, SweepSouth

For the full GitLab Commit London program, please visit the [schedule](https://about.gitlab.com/events/commit/#schedule?utm_medium=pressrelease&utm_campaign=commitlondon).
 
“GitLab is one of the most important tools for our engineering, it's the central hub of all our operations. This event is the perfect opportunity to learn from how others are using the power that GitLab offers." - Alexandru Constantin Viscreanu, Platform Engineer at Kiwi.com
 
**Conference Location and Registration**
 
In London, where history meets innovation, venture down a cobblestone drive, alongside leaders who do contrive to embrace a world of automation, exploration, and transformation. Your journey will begin when you arrive at a brewery to derive the benefits of collaboration. A day of learning and excitement: GitLab Commit sparks curiosity to learn techniques to increase velocity. You’ll share and learn insight to shape the future of development And enjoy a mini- golf party in the moonlight. Register by Sept 6th, 11:59 pm PT with code: commit99 to save $99 on conference passes.
 
**Thank You Sponsors**
 
GitLab Commit is made possible with support from Sponsors: AWS, CNCF, FINOS, and StackOverflow, and Media Sponsors: DevOps.com and Software Engineering Daily. For more information on sponsorship opportunities, please reach out to commit@gitlab.com.

#### About GitLab
GitLab is a DevOps platform built from the ground up as a single application for all stages of the DevOps lifecycle enabling Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle, allowing teams to collaborate and work on a project from a single conversation, significantly reducing cycle time and focus exclusively on building great software quickly. Built on open source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network, and Comcast trust GitLab to deliver great software at new speeds.

#### Media Contact
Natasha Woods
<br> 
GitLab
<br> 
nwoods@gitlab.com
<br>
(415) 312-5289