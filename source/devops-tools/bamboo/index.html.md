---
layout: markdown_page
title: "Atlassian Bamboo"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Bamboo Server is a CI/CD solution which is part of the Atlassian suite of developer tools. It is available only in a self-managed configuration and is a closed source solution. Bamboo offers build, test, and deployment automation, and has tight integrations to Atlassian Bitbucket (for SCM) and Fisheye (for understanding how source code has changed), as well as integrations to over 150 other tools. In contrast, GitLab offers a git-based SCM, SCM integrations, and code change traceability out of the box in a single application.

Bamboo offers a GUI for defining build plans, but does not offer pipeline as code. Bamboo also offers deployment plans (which include the notion of environments and releases), pre-deployment visibility, and per-environment deployment permissions. GitLab also offers release tracking across environments and deep visibility into the changes in a deployment, but sets deployment permissions based on branch permissions.

Bamboo does not offer monitoring. GitLab includes monitoring as part of its single application.

Bamboo steps can be run in parallel across agents, and those agents can be auto-scaled based on need if Bamboo is configured for a feature called Elastic Bamboo. [Elastic Bamboo](https://confluence.atlassian.com/bamboo/configuring-elastic-bamboo-289277120.html) requires the use of "remote agents", which you pay extra for (see [pricing](#pricing)). Organizations who want auto-scaling are also locked in to using Amazon Elastic Compute Cloud (EC2) and paying Amazon separately for their usage. In contrast, GitLab does not charge per remote agent (runner) and scales with a variety of cloud and container solutions.

## Weaknesses
* Extending the native functionality of Bamboo is done through plugins. Plugins are expensive to maintain, secure, and upgrade. In contrast, GitLab is [open core](https://about.gitlab.com/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change. 

## Comments/Anecdotes
* Discussion from [HackerNews article about Atlassian not allowing benchmarking](https://news.ycombinator.com/item?id=18103162#18103813)
   > Atlassian has always forbidden to talk about the performance of their products in their ToS and in their previous EULA. We all know why, but we don’t talk about it.
- Sales heard from large networking company
   > "Terrible!, All UI Based, Cannot configure 'as code' (ala .gitlab-ci.yml)"

- Sales previous experience)
   > Previously we beat Bamboo by the reason that it’s too prescripted for build only.

   This is likely no longer true as they now have deployment specific features including environments, deployment groups, and per-environment deployment permissions

- From Twitter:

   - "we have also started using GitLab (moving from our own Bitbucket/Bamboo servers). The CI/CD is definitely an improvement but I'm not sold on the code review features of GitLab"  
     [https://twitter.com/carrchr/status/1003651960099176448](https://twitter.com/carrchr/status/1003651960099176448)

   - "Sweet. Unlike Atlassian's Bamboo, @GitLab CI supports "[ci skip]" out of the box. #gitlab #devtools #success"  
     [https://twitter.com/tekkie/status/823689378371342336](https://twitter.com/tekkie/status/823689378371342336)

   - "Nice and simple GUI in @GitLab CI as well. Much easier to navigate than Bamboo for instance. #gitlab #gui #ux #success"  
     [https://twitter.com/tekkie/status/839054544009117696](https://twitter.com/tekkie/status/839054544009117696)

   - "Seriously, is still there a reason to use Jenkins/Hudson/TeamCity/Bamboo? I reckon @GitLab built-in CI support is sufficient for most of us!"  
     [https://twitter.com/AriyaHidayat/status/756919101587546112](https://twitter.com/AriyaHidayat/status/756919101587546112)

   - "A day building pipelines in VSTS for one of my teams. Get the feeling that while I find it simple not everyone finds it the same. Better than Gitlab CI (IMO) but I like Bamboo a lot too..."  
     [https://twitter.com/xyglo/status/978291270161457152](https://twitter.com/xyglo/status/978291270161457152)

- From Bamboo open Issues

   - **Issue:** If I want to use git submodules then I shouldn't have to upload and configure SSH keys on each Bamboo Agent.
     - **Key text:** "Bamboo requires separate Git authentication for submodules. This involves either using HTTPS for submodules and providing the credentials through the job's environment variables, or configuring separate SSH keys on each build agent. Using HTTPS would render local builds unusable, requiring credentials every time. Adding SSH keys to every Bamboo agent is unmaintainable. . . . This reason, among others is a large part of why we have migrated away from Bamboo. We now use Gitlab and Gitlab CI for much better Docker and git support."
     - **Link:** [Bamboo Issue in Jira](https://jira.atlassian.com/browse/BAM-11369?focusedCommentId=1588832&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-1588832)

## Resources
* [Atlassian Bamboo Website](https://www.atlassian.com/software/bamboo)

## Pricing
- [Price page](https://www.atlassian.com/software/bamboo/pricing)
- [Bamboo Pricing Guide](https://www.atlassian.com/licensing/bamboo#serverlicenses-3)  
   (includes price additions for remote agents, and academic pricing)
- Small Teams - $10/month - only 10 jobs and no remote agents
- Growing Teams - $880/month - unlimited jobs, 1 remote agent
- pricing increase in tiers by # remote agents (1, 5, 10, 25, 100, 250, 500, 1000)
   (see Bamboo Pricing Guide for prices)
- First purchase includes perpetual software and 1 yr maintenance. Yearly cost for maintenance is approximately 50% of initial remote agent tier cost. (e.g. 1st year @25 remote agents = $8,800, second year maintenance = $4,400)

## Comparison
