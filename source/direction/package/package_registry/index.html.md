---
layout: markdown_page
title: "Category Vision - Package Registry"
---

- TOC
{:toc}

## Package Registry

As part of our overall vision for packaging at GitLab, we want to provide a single interface for managing dependencies,
registries, and package repositories. Whether C/C++, Java, Ruby, or any other language, our vision is that you can
store your binaries and dependencies in the GitLab Package Registry. There are a few key package/language types on our radar
that we want to support as part of this category:

- **C/C++** developers are an important group of users who need these capabilities, and [Conan](https://conan.io) is an open-source solution that is already available and has been requested by GitLab users. By integrating with Conan, GitLab will enable C/C++ developers to build, publish and share all of their dependencies, all from within GitLab. 
- Linux distros depend on linux package regisitries for distribution of installable software. By supporting **Debian** and **RPM** packages we will cater to a big chunk of the market and allow [Systems Administrator](https://design.gitlab.com/research/personas#persona-sidney) tasks to be internalized.
- Maven helps **Java** developers to simplify the build process by providing a uniform build system to streamline development. The GitLab Maven integration provides a standardized method of sharing and versioning Java dependencies across projects.
- NPM allows developers to access **JavaScript** packages to help drive faster development. The GitLab NPM Registry allows developers to publish, share and version control all of their packages right alongside their source code.
- A Rubygem registry offers **Ruby** developers of lower-level services to provision an easy to use, integrated solution to share and version control ruby gems in a standardized and controlled way. Being able to provision them internally sets projects up for improved privacy and pipeline build speeds.
- For **C#/.NET** developers, we plan to integrate with the [NuGet](https://www.nuget.org/) repository system to make developing .NET apps much easier.

Because there are many great solutions out there in the market for package registries, our plan is to provide a single, consistent interface to whichever package manager you're using. We want you to be able to take advantage of the great features each of these tools offer, but at the same time, we want there to be a consistent experience that's well integated with the rest of your workflow at GitLab.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3APackage+Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

Next up on our agenda is to implement [gitlab-ee#8248](https://gitlab.com/gitlab-org/gitlab-ee/issues/8248), the MVC for the GitLab Conan Repository, which will provide a base feature for users to start integrating with and providing feedback.

We are also continuing to improve our existing NPM and Maven integrations. For Maven, [gitlab-ee#9947](https://gitlab.com/gitlab-org/gitlab-ee/issues/9947) will add support for pulling dependencies at the sub-group level. For NPM, [gitlab-ee#9104](https://gitlab.com/gitlab-org/gitlab-ee/issues/9960) will allow users to authenticate with `CI_JOB_TOKEN` and help streamline publishing packages via GitLab CI. Finally, [gitlab-ee#10003](https://gitlab.com/gitlab-org/gitlab-ee/issues/10003) will expand the [packages API](https://docs.gitlab.com/ee/api/packages.html) to allow users to list all packages of all projects within a given group in a single list.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- Resolve NPM authentication and adoption issues
  - [Pull NPM package from sub-group/project](https://gitlab.com/gitlab-org/gitlab-ee/issues/9960) (Complete)
  - [Authenticate with GitLab personal access token](https://gitlab.com/gitlab-org/gitlab-ee/issues/9140) (Complete)
  - [Authenticate with CI_JOB_TOKEN](https://gitlab.com/gitlab-org/gitlab-ee/issues/9104)
- Conan C/C++ Package Manager
  - [Support C/C++ Developers by adding Conan to the Package Registry](https://gitlab.com/gitlab-org/gitlab-ee/issues/8248)
  - [Authenticate with CI_JOB_TOKEN](https://gitlab.com/gitlab-org/gitlab-ee/issues/11678)
  - [Project and Group level support for Conan Repository](https://gitlab.com/gitlab-org/gitlab-ee/issues/11679)
- Maven Repository
  - [Pull Maven dependencies from sub-group/project](https://gitlab.com/gitlab-org/gitlab-ee/issues/9947)

## Competitive Landscape

|         | GitLab | Artifactory | Nexus | GitHub |
| ------- | ------ | ----------- | ----- | ------ |
| Conan   | - | ✔️ | ☑️ | - |
| Linux   | - | ✔️ | ✔️ | - |
| Maven   | ✔️ | ✔️ | ✔️ | ☑️ |
| NPM     | ✔️ | ✔️ | ✔️ | ☑️ |
| NuGet   | - | ✔️ | ✔️ | ☑️ |
| RubyGem | - | ✔️ | ✔️ | ☑️ |
| Swift   | - | ✔️ | ✔️ | ☑️ |

☑️ _indicates support is through community plugin or beta feature_

Historically, we've provided access to the GitLab container registry for free, but limited access to package registries to our paid tier. However, with [GitHub's annoucement](https://github.blog/2019-05-10-introducing-github-package-registry/) to support a package registry for free, we are re-evaluating this strategy. 

## Top Customer Success/Sales Issue(s)

- The top customer success and sales issue for NPM is [gitlab-ee#9104](https://gitlab.com/gitlab-org/gitlab-ee/issues/9104) which has been requested by several customers looking to use the CI_JOB_TOKEN to authenticate to the NPM Registry via GitLab CI.
- For Maven, the top customer success and sales issues are related to improving and expanding the UI. [#ee-7932](https://gitlab.com/gitlab-org/gitlab-ee/issues/7932) seeks to improve the navigation for the entire GitLab registry and standardize our naming conventions. 

## Top Customer Issue(s)

- For Maven, our top customer issues are related to simplifying our API protocols and improving how we handle permissions. [gitlab-ee#8280](https://gitlab.com/gitlab-org/gitlab-ee/issues/8280) focuses on making our API more user-friendly by eliminating the need to translate project_name to project_id. [gitlab-ee#10749](https://gitlab.com/gitlab-org/gitlab-ee/issues/10749#) seeks to expand authentication to include deploy tokens.
- Our TAM (technical account management) team has identified [adding Support for Helm chart registry](https://gitlab.com/gitlab-org/gitlab-ce/issues/35884) as an issue customers are looking for.
- For NPM, the TAMs have selected [Authenticate with CI_JOB_TOKEN to NPM registry](https://gitlab.com/gitlab-org/gitlab-ee/issues/9104) as most important.

## Top Internal Customer Issue(s)

- The Distribution team utilizes an external tool for building Linux packages. By offering support for Debian and RPM we can begin to remove that external dependency. 
- The GitLab Distribution team also utilizes rubygems for downloading external dependencies. They would like to speed up their build times and remove their reliance on external dependencies by caching frequently used packages. This would require an integration with rubygems as well as the dependency proxy. [gitlab-ce#225](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/225) details their needs and requirements. 

## Top Vision Item(s)

A lot of moving our vision forward is integrating MVC support for new languages and package types:

### C/C++

Our vision begins with implementing the [gitlab-ee#8248](https://gitlab.com/gitlab-org/gitlab-ee/issues/8248),
which will allow us to kick off support. Beyond the initial MVC implementation, our
focus will be on improving consistency and core features across all of our repository
types rather than providing C/C++ specific features. This may change as we begin
to get customer feedback from real-world users of the new repository.

### .NET

For .NET, the Microsoft-supported mechanism for sharing code is NuGet, which defines how packages for .NET are created, hosted, and consumed, and provides the tools for each of those roles. By integrating with NuGet, GitLab will provide a centralized location to store and view those packages, in the same place as their source code and pipelines. [gitlab-ce#39794](https://gitlab.com/gitlab-org/gitlab-ce/issues/39794) details the first steps in adding NuGet support to the GitLab Package Registry.

### Linux Packages

[gitlab-ee#5835](https://gitlab.com/gitlab-org/gitlab-ee/issues/5835) and [gitlab-ee#5932](https://gitlab.com/gitlab-org/gitlab-ee/issues/5932), which relate to adding support for Debian and RPM respectively.

### RubyGems

[gitlab-ee#803](https://gitlab.com/gitlab-org/gitlab-ee/issues/803) which will add support for a RubyGem registry.

### Helm Charts

[gitlab-ee#9993](https://gitlab.com/gitlab-org/gitlab-ee/issues/9993) details the need to track which underlying packages a release incorporates and detail any changes in those underlying dependencies. Although not specific to Helm charts, this functionality represents a leap forward in our current offering. 
