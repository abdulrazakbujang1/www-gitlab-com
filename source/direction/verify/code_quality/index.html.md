---
layout: markdown_page
title: "Category Vision - Code Quality"
---

- TOC
{:toc}

## Code Quality

Automatically analyze your source code to surface issues and see if quality is improving or getting worse with the latest commit.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ACode%20Quality)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1302) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Up next for code quality is [gitlab-#4189](https://gitlab.com/gitlab-org/gitlab/issues/4189) which allows a more detailed view of code quality report in an MR.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Code quality on `master`](https://gitlab.com/gitlab-org/gitlab-ee/issues/2766)
- [CI view for code quality](https://gitlab.com/gitlab-org/gitlab-ee/issues/4189)
- [Code quality notices on diffs/MRs](https://gitlab.com/gitlab-org/gitlab-ee/issues/2526)

## Competitive Landscape

### Azure DevOps

Azure DevOps does not offer in-product quality testing in the same way we do with CodeClimate, but does have a number of easy to find and install plugins in their [marketplace](https://marketplace.visualstudio.com/search?term=code%20quality&target=AzureDevOps&category=All%20categories&sortBy=Relevance) that are both paid and free. Their [SonarQube plugin](https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarqube) appears to be the most popular, though it seems to have some challenges with the rating.

In order to remain ahead of Azure DevOps, we should continue to push forward the feature capability of our own open-source integration with CodeClimate. Issues like [gitlab-ee#2766](https://gitlab.com/gitlab-org/gitlab-ee/issues/2766) (per-branch view for how code quality progresses over time) moves both our vision forward as well as ensures we have a high quality integration in our product. To be successful here, though, we need to support formats Microsoft-stack developers use: [gitlab-ce#62727](https://gitlab.com/gitlab-org/gitlab-ce/issues/62727) gets the ball rolling. Because CodeClimate does not yet have deep .NET support, we may need to build something ourselves.

## Top Customer Success/Sales Issue(s)

An interesting suggestion from the CS team is [gitlab-ee#8406](https://gitlab.com/gitlab-org/gitlab-ee/issues/8406), which is beyond just code quality but code quality could lead the way. This item introduces instance wide code statistics, showing (for example) programming languages used, code quality statistics, and code coverage.

## Top Customer Issue(s)

The most popular item is [gitlab-ee#14405](https://gitlab.com/gitlab-org/gitlab-ee/issues/14405), which would provide an easier view of the change in coverage percentage between an MR and the target branch.

## Top Internal Customer Issue(s)

Showing code coverage delta in the MR widget ([gitlab-ee#2526](https://gitlab.com/gitlab-org/gitlab-ee/issues/2526#)) is a useful feature for internal users that will help us make better decisions about if a merge request should be merged, or if it needs further refinement.

## Top Vision Item(s)

In terms of moving the vision forward, [gitlab-ee#2766](https://gitlab.com/gitlab-org/gitlab-ee/issues/2766) (which creates a per-branch view for code quality) is an interesting approach. This brings code quality tracking out of the MR widget, and looks at how code quality is changing over a longer period of time in the context of a branch (or master.)

We also want to provide better support for .NET users of GitLab. [gitlab-ce#62727](https://gitlab.com/gitlab-org/gitlab-ce/issues/62727) adds support for more traditional Microsoft stack code coverage tools, making GitLab just as competent as Azure DevOps at showing code coverage.
