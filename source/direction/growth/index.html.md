---
layout: markdown_page
title: Product Vision - Growth
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

The [Growth stage](https://about.gitlab.com/handbook/product/categories/#growth-stage) is responsible for 
scaling GitLab's business value. To accomplish this, Growth analyzes the entire customer journey from acquisition of 
a customer to the flow across multiple GitLab features - and even reactivation of lost users. Several groups 
help with this mission:
* [Acquisition](https://about.gitlab.com/direction/growth/acquisition/), [Conversion](https://about.gitlab.com/direction/growth/conversion/), [Expansion](https://about.gitlab.com/direction/growth/expansion/) and [Retention](https://about.gitlab.com/direction/growth/retention/) connect users to the existing value that GitLab already delivers by
rapid experimentation.
* [Fulfillment](https://about.gitlab.com/direction/fulfillment/) ensures a terrific experience throughout the 
buyer journey for customers doing business with GitLab.
* [Telemetry](https://about.gitlab.com/direction/telemetry/) builds the backbone of data that other groups need to be successful, enabling a data-informed product 
culture at GitLab.

Growth is commonly asking and finding solutions for these types of questions:

* Why are so many users signing up for GitLab but never logging in?
* How do we successfully attract new customers to GitLab?
* How do we successfully win back customers that once used and/or paid for GitLab but have canceled their subscription?
* What is the first action taken after logging in and is it the right one for maximum connection to value?
* How can we grow the amount of customers that use all stages of GitLab?

### Why Invest in a Growth Section in 2019?

We believe we have found product-market fit by providing a single application
for the entire DevOps lifecycle, highlighting the value created when teams don’t
have to spend time integrating disparate tools. Now that product-market fit has
been achieved, we must do a better job of connecting our value to our customers.
Therefore, a Growth focus is required. Additionally, we are beginning to put
more strategic focus on the growth of GitLab.com, which is a different user
signup and activation process than self-managed instances, which typically
involves direct sales.

## Acquisition, Conversion, Expansion, and Retention groups

While the Growth function is becoming more common in late stage, well funded
software companies, it is still a relatively nascent discipline. At GitLab, we
seek to derive best practices from the industry and adapting them to work with
our culture. Here are a few articles we’ve found helpful as we are formulating a
Growth strategy and vision:

* [What Are Growth Teams For, and What Do They Work On?](https://news.greylock.com/what-are-growth-teams-for-and-what-do-they-work-on-a339d0c0dee3)
* [Sustainable Product Growth](https://www.sequoiacap.com/article/sustainable-product-growth)

The Acquisition, Conversion, Expansion and Retention groups are GitLab's approach to a dedicated, 
experiment-driven Growth team. These groups are expected to always start with a hypothesis. 
The hypothesis should be fully defined prior to execution in order to safeguard from "bad 
science," namely data that may correlate but not have any causal relationship. After a hypothesis has been
defined, Growth relies on two primary constructs to make decisions: analysis of data, or
what people do in the application, and qualitative research, in essence the
“why” behind the data.

Growth should focus on metrics that are important at the company level, while also considering
company strategy. Each group will own a north star KPI which are linked [here](https://about.gitlab.com/handbook/product/metrics/).

Acquisition, Conversion, Expansion and Retention will consist of a product manager, engineering manager,
some full-stack engineers, a (shared) data analyst, and a (shared) Product Designer.
Each group member will have a functional reporting structure, in order to report
to a leader that understands their job function.

### Weekly Growth meeting
The Growth Product Management Director should run a weekly growth meeting to review, prioritize, and plan experiments.  The meeting should take place on Tuesdays for 50 min and should cover the following agenda.
15 min: metrics review
10 min: last week’s testing activity review (update on live experiments) 
15 min: lessons learned 
15 min: select this week’s tests
5 min: check on idea backlog

To prepare for the meeting, the Growth PM's and the Growth Director should check in on experiments to identify which can be concluded, and to collect data for review.

### Growth ideation and prioritization
Anyone is welcome to submit ideas offline to idea backlog as issues (name, description, hypothesis, metrics to measure, and ICE score).

To prioritize, the growth groups will use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64), which consists of the following elements, scored on a scale of 1-10:
* Impact - high score for high impact
* Confidence - high score for high confidence
* Ease - high score for ease

### Working across stages

The hypotheses explored by Growth will span across groups and stages. How do other groups interact with Growth?

Growth does not own areas of the product, but finds ways to help users discover and unlock value throughout the GitLab
application. They will do a combination of shipping low-hanging fruit themselves, but will partner with other stages
on larger initiatives that need specialized knowledge or ongoing focus.

It is the responsibility of other stages to maintain and expand core value in their respective product areas; it's
Growth's mission to streamline delivery of that value to users. For example:

* [Manage](https://about.gitlab.com/handbook/product/categories/#manage-stage) is responsible for user registration and
management. This stage maintains a long-term vision on how users are created and maintained in GitLab, with a roadmap that
includes other categories and priorities.
* Growth may identify user registration as an opportunity to further the group's [priorities](#growth-priorities). Growth's
engineering team may ship smaller improvements independently of Manage's direct involvement during implementation (such as [soft email confirmation](https://gitlab.com/gitlab-org/growth/product/issues/7)), but may need to partner with Manage on larger efforts like a completely [new onboarding](https://gitlab.com/gitlab-org/gitlab-ce/issues/57832) experience.
* The group ultimately owning the change - in this case, Manage - would review Growth's contributions into their area of the product 
and - like a contribution coming from the wider community - ultimately own the final result.

Presenting the relationship in a [RACI matrix](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix):

| Task | Growth Product/Engineering | Data Team | Product Leadership | Stage Product/Engineering |
| ------ | ------ | ------ | ------ | ------ |
| Hypothesis Generation | R | I | A | C |
| Experiment Design | R | C | A | I |
| Experiment Implementation | R | I | A | C |
| Contribution Review | A | |  | R |
| Experiment Ramp | R | C | A | I |
| Post-Experiment Analysis | C | R | I | I |
| Post-Experiment Decision | R | C | C | C |
| Maintenance | C | |  | R |


## Fulfillment group

Responsible for licensing & billing workflows, which have a huge impact on renewal rates, upgrade rates, and customer satisfaction, and represent important levers for the Growth team.

Please see the [Fulfillment direction page](https://about.gitlab.com/direction/fulfillment/) for more 
information about Fulfillment's mission, vision, and priorities.

## Telemetry group

Responsible for product usage data, including proper instrumentation, working with BizOps and Engineering to ensure product usage data is stored correctly, and working across the Product team to ensure product usage data is analyzed and visualized in a way that can drive product and growth decisions.

Please see the [Telemetry direction page](https://about.gitlab.com/direction/telemetry/) for more 
information about Telemetry's mission, vision, and priorities.
